﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authorization;
using reserveapp_api.Models;
using reserveapp_api.AuthorizationPolicies;
using reserveapp_api.Services;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using reserveapp_api.Models.SystemModels;

namespace reserveapp_api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var sqlConnectionString = Environment.GetEnvironmentVariable("DB_CONNECTION_STRING");
            if (sqlConnectionString == null)
            {
                sqlConnectionString = Configuration.GetConnectionString("NpgsqlConnection");
            }
            services.AddDbContext<AppDbContext>(options => options.UseNpgsql(sqlConnectionString));

            //JWT Tokens. set token validation parameters
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    //установка ключа безопасности
                    IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
                    //валидация ключа безопасности
                    ValidateIssuerSigningKey = true,
                    //будет ли валидироваться время существования
                    ValidateLifetime = true,

                    //При валидации издателя и потребителя аутентификация падает, возможно при описании токена забивать издателя и потребителя (Возможно проблема в том, что в Payload не было полей)
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            //Add custom authorization handlers JWT
            services.AddAuthorization(options =>
            {
                options.AddPolicy("WhiteTokensOnly", policy => policy.Requirements.Add(new WhiteTokenRequirement()));
            });

            services.AddTransient<IAuthorizationHandler, IsWhiteTokenAuthorizationHandler>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "wwwroot";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (!Directory.Exists(Path.Combine(Directory.GetCurrentDirectory(), "Resources")))
                Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), "Resources"));
            if (!Directory.Exists(Path.Combine(Directory.GetCurrentDirectory(), "Resources", "BranchImages")))
                Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), "Resources", "BranchImages"));
            if (!Directory.Exists(Path.Combine(Directory.GetCurrentDirectory(), "Resources", "ZoneImages")))
                Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), "Resources", "ZoneImages"));



            //Set default data in database
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            using (var context = serviceScope.ServiceProvider.GetRequiredService<AppDbContext>())
            {
				context.Database.EnsureCreated();
				context.Database.Migrate();
                context.EnsureSeedData();
            }

            app.Use(async (context, next) =>
            {
                await next();
                if (context.Response.StatusCode == 404 && !Path.HasExtension(context.Request.Path.Value) &&
                    !context.Request.Path.Value.StartsWith("/api/") && !context.Request.Path.Value.StartsWith("/Resources/"))
                {
                    context.Request.Path = "/index.html";
                    await next();
                }
            });

            //Аутентификация (JWT Tokens)
            app.UseAuthentication();
            app.UseMvc();
            //For the static files not in wwwroot, Folder - resources
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(
                Path.Combine(Directory.GetCurrentDirectory(), @"Resources")),
                RequestPath = new PathString("/Resources")
            });


            if (env.IsDevelopment()) { app.UseDeveloperExceptionPage(); }
            //else { app.UseHsts(); }

            app.UseDefaultFiles();
            app.UseStaticFiles();   //For the wwwroot folder
            //app.UseHttpsRedirection();
            app.UseSpaStaticFiles();
            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "wwwroot";
            });


            //Bot Configurations
            //Bot.GetBotClientAsync().Wait();
        }
    }
}
