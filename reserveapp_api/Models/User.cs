using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace reserveapp_api.Models
{
    public class User
    {
        public User() { }

        public long Id { get; set; }
        public Guid Guid { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public bool IsEmailConfirmed { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Salt { get; set; }
        public double Score { get; set; }
        [Required]
        public virtual Company Company { get; set; }
    }
}
