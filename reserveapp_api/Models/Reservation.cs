﻿using System;
using System.ComponentModel.DataAnnotations;

namespace reserveapp_api.Models
{
    public class Reservation
    {
        public long Id { get; set; }
        [Required]
        public virtual Branch Branch { get; set; }
        [Required]
        public DateTime Time { get; set; }
        [Required]
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public string UserPhone { get; set; }

        public ReservationStatus Status { get; set; }

        public Reservation() { }

        public Reservation(Reservation data)
        {
            Id = data.Id;
            Branch = data.Branch;
            Time = data.Time;
            UserName = data.UserName;
            UserEmail = data.UserEmail;
            UserPhone = data.UserPhone;
            Status = data.Status;
        }
    }
}
