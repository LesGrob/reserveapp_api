﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using reserveapp_api.DTO;

namespace reserveapp_api.Models
{
    public class Branch
    {
        public long Id { get; set; }
        [Required]
        public Guid Guid { get; set; }
        [Required]
        public string Name { get; set; }
        public string Adress { get; set; }
        public string Phone { get; set; }
        public bool Tester { get; set; }
        public bool TelegramIsAvailiable { get; set; }
        [Required]
        public virtual Company Company { get; set; }
        [Required]
        public virtual Tariff Tariff { get; set; }
        [Required]
        public virtual Schedule Schedule { get; set; }

        public string ImagePath { get; set; }

        public Branch()
        {
            Guid = Guid.NewGuid();
        }

        public Branch(Branch data)
        {
            Id = data.Id;
            Guid = data.Guid;
            Name = data.Name;
            Adress = data.Adress;
            Phone = data.Phone;
            Company = data.Company;
            Tariff = data.Tariff;
            Schedule = data.Schedule;
            TelegramIsAvailiable = data.TelegramIsAvailiable;
            ImagePath = data.ImagePath;
        }

        public Branch(Branch_UploadDTO data)
        {
            Id = data.Id;
            Guid = data.Guid;
            Name = data.Name;
            Adress = data.Adress;
            Phone = data.Phone;
            Company = data.Company;
            Tariff = data.Tariff;
            Schedule = new Schedule(data.Schedule);
            TelegramIsAvailiable = data.TelegramIsAvailiable;
            ImagePath = data.ImagePath;
        }
    }
}
