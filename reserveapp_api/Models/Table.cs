﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace reserveapp_api.Models
{
    public class Table
    {
        public long Id { get; set; }
        [Required]
        public virtual Zone Zone { get; set; }

        public string Description { get; set; }
        public int Sits { get; set; }
        public int DepositCost { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Radius { get; set; }
        public int PositionX { get; set; }
        public int PositionY { get; set; }


        public Table() { }

        public Table(Table data)
        {
            Id = data.Id;
            Zone = data.Zone;
            Description = data.Description;
            Sits = data.Sits < 1 ? 1 : data.Sits;
            DepositCost = data.DepositCost < 0 ? 0 : data.DepositCost;
            Width = data.Width < 1 ? 1 : data.Width;
            Height = data.Height < 1 ? 1 : data.Height;
            Radius = data.Radius < 1 ? 1 : data.Radius;
            PositionX = data.PositionX < 1 ? 1 : data.PositionX;
            PositionY = data.PositionY < 1 ? 1 : data.PositionY;
        }

        public void TableClone(Table data)
        {
            Id = data.Id;
            Zone = data.Zone;
            Description = data.Description;
            Sits = data.Sits < 1 ? 1 : data.Sits;
            DepositCost = data.DepositCost < 0 ? 0 : data.DepositCost;
            Width = data.Width < 1 ? 1 : data.Width;
            Height = data.Height < 1 ? 1 : data.Height;
            Radius = data.Radius < 1 ? 1 : data.Radius;
            PositionX = data.PositionX < 1 ? 1 : data.PositionX;
            PositionY = data.PositionY < 1 ? 1 : data.PositionY;
        }
    }
}
