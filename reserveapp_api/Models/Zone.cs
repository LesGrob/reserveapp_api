﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace reserveapp_api.Models
{
    public class Zone
    {
        public long Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public Guid Guid { get; set; }

        public bool Temporary { get; set; }
        [Required]
        public virtual Branch Branch { get; set; }

        public string ImagePath { get; set; }
        public int ImageWidth { get; set; }
        public int ImageHeight { get; set; }
        public string Color { get; set; }

        public string TableColor { get; set; }
        public string TableBColor { get; set; }

        public string TableActiveColor { get; set; }
        public string TableActiveBColor { get; set; }

        public string TableDisabledColor { get; set; }
        public string TableDisabledBColor { get; set; }

        public Zone() { Guid = Guid.NewGuid(); Temporary = false; }

        public Zone(Zone data)
        {
            Id = data.Id;
            Name = data.Name;
            Guid = data.Guid;
            Temporary = data.Temporary;
            Branch = data.Branch;
            ImagePath = data.ImagePath;
            ImageWidth = data.ImageWidth;
            ImageHeight = data.ImageHeight;
            Color = data.Color;

            TableColor = data.TableColor;
            TableBColor = data.TableBColor;

            TableActiveColor = data.TableActiveColor;
            TableActiveBColor = data.TableActiveBColor;

            TableDisabledColor = data.TableDisabledColor;
            TableDisabledBColor = data.TableDisabledBColor;
        }

        public static bool CorrectColor(String color)
        {
            return String.IsNullOrEmpty(color) || Regex.IsMatch(color, @"(#([\da-f]{3}){1,2}|(rgb|hsl)a\((\d{1,3}%?,\s?){3}(1|0?\.\d+)\)|(rgb|hsl)\(\d{1,3}%?(,\s?\d{1,3}%?){2}\))");
        }

        public void ZoneClone(Zone data)
        {
            Id = data.Id;
            Name = data.Name;
            //Guid = data.Guid;
            Temporary = data.Temporary;
            Branch = data.Branch;
            ImagePath = data.ImagePath;
            ImageWidth = data.ImageWidth;
            ImageHeight = data.ImageHeight;
            Color = data.Color;

            TableColor = data.TableColor;
            TableBColor = data.TableBColor;

            TableActiveColor = data.TableActiveColor;
            TableActiveBColor = data.TableActiveBColor;

            TableDisabledColor = data.TableDisabledColor;
            TableDisabledBColor = data.TableDisabledBColor;
        }
    }
}
