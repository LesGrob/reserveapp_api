using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Npgsql;

namespace reserveapp_api.Models
{
    /// <summary>
    /// Зерно БД
    /// </summary>
    public static class DatabaseStoreExtensions
    {
        /// <summary>
        /// Заполнить БД базовыми значениями
        /// </summary>
        /// <param name="context">Контекст подключения к БД</param>
        public static void EnsureSeedData(this AppDbContext context)
        {
            if (!context.Tariffs.Any())
            {
                context.Tariffs.AddRange(
                    new Tariff() { Name = "Стандартный", Period = 1, Price = 500, TablePrice = 100, IsAvailiable = true, BotPrice = 50 },
                    new Tariff() { Name = "Долговременный", Period = 6, Price = 400, TablePrice = 50, IsAvailiable = true, BotPrice = 50 });
                context.SaveChanges();
            }

            if (!context.EmailDomains.Any())
            {
                context.EmailDomains.AddRange(
                    new EmailDomain("mail.ru", "Почта Mail.Ru", "https://e.mail.ru/"),
                    new EmailDomain("bk.ru", "Почта Mail.Ru (bk.ru)", "https://e.mail.ru/"),
                    new EmailDomain("list.ru", "Почта Mail.Ru (list.ru)", "https://e.mail.ru/"),
                    new EmailDomain("inbox.ru", "Почта Mail.Ru (inbox.ru)", "https://e.mail.ru/"),
                    new EmailDomain("yandex.ru", "Яндекс.Почта", "https://mail.yandex.ru/"),
                    new EmailDomain("ya.ru", "Яндекс.Почта", "https://mail.yandex.ru/"),
                    new EmailDomain("yandex.ua", "Яндекс.Почта", "https://mail.yandex.ua/"),
                    new EmailDomain("yandex.by", "Яндекс.Почта", "https://mail.yandex.by/"),
                    new EmailDomain("yandex.kz", "Яндекс.Почта", "https://mail.yandex.kz/"),
                    new EmailDomain("yandex.com", "Yandex.Mail", "https://mail.yandex.com/"),
                    new EmailDomain("gmail.com", "Gmail", "https://mail.google.com/"),
                    new EmailDomain("googlemail.com", "Gmail", "https://mail.google.com/"),
                    new EmailDomain("outlook.com", "Outlook.com", "https://mail.live.com/"),
                    new EmailDomain("hotmail.com", "Outlook.com (Hotmail)", "https://mail.live.com/"),
                    new EmailDomain("live.ru", "Outlook.com (live.ru)", "https://mail.live.com/"),
                    new EmailDomain("live.com", "Outlook.com (live.com)", "https://mail.live.com/"),
                    new EmailDomain("me.com", "iCloud Mail", "https://www.icloud.com/"),
                    new EmailDomain("icloud.com", "iCloud Mail", "https://www.icloud.com/"),
                    new EmailDomain("rambler.ru", "Рамблер-Почта", "https://mail.rambler.ru/"),
                    new EmailDomain("yahoo.com", "Yahoo! Mail", "https://mail.yahoo.com/"),
                    new EmailDomain("ukr.net", "Почта ukr.net", "https://mail.ukr.net/"),
                    new EmailDomain("i.ua", "Почта I.UA", "http://mail.i.ua/"),
                    new EmailDomain("bigmir.net", "Почта Bigmir.net", "http://mail.bigmir.net/"),
                    new EmailDomain("tut.by", "Почта tut.by", "https://mail.tut.by/"),
                    new EmailDomain("inbox.lv", "Inbox.lv", "https://www.inbox.lv/"),
                    new EmailDomain("mail.kz", "Почта mail.kz", "http://mail.kz/")
                    );
                context.SaveChanges();
            }
        }

        private static string ComputePasswordHash(string password, string salt)
        {
            SHA512 sha = SHA512.Create();
            //Пароль + соль
            string valToHash = password + salt;
            byte[] bytes = Encoding.Default.GetBytes(valToHash);
            byte[] hashBytes = sha.ComputeHash(bytes);
            string hash = "";
            for (int i = 0; i < hashBytes.Length; i++)
                hash += hashBytes[i].ToString("X");
            return hash;
        }
    }
}
