using System;

namespace reserveapp_api.Models
{
    public class UserToken
    {
        public UserToken() { }

        public long Id { get; set; }
        public User user { get; set; }
        public string Token { get; set; }
        public DateTimeOffset DateIssue { get; set; }
        public DateTimeOffset DateEnd { get; set; }
    }
}
