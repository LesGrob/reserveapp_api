using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace reserveapp_api.Models
{
    public class Company
    {
		public Company() { }

        public long Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string INN { get; set; }
        public string OGRN { get; set; }
        public bool IsIP { get; set; }
        [Required]
        public bool IsConfirmed { get; set; }
    }
}
