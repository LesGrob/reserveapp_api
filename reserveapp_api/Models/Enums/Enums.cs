﻿using System;
namespace reserveapp_api.Models
{
    public enum BranchStatus
    {
        NotPaid = 1,
        Paid = 2,
        Expired = 3
    }

    public enum ReservationStatus
    {
        Finished = 1,
        Active = 2,
        Disabled = 3
    }

    public enum OrderStatus
    {
        Accepted = 1,
        Cancelled = 2
    }

    public enum WeekDay
    {
        Sunday = 0,
        Monday = 1,
        Tuesday = 2,
        Wednesday = 3,
        Thursday = 4,
        Friday = 5,
        Saturday = 6
    }
}
