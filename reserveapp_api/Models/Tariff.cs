﻿using System.ComponentModel.DataAnnotations;

namespace reserveapp_api.Models
{
    public class Tariff
    {
        public Tariff() { }

        public long Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public double Price { get; set; }
        [Required]
        public double TablePrice { get; set; }
        [Required]
        public double BotPrice { get; set; }
        //Period months count
        [Required]
        public int Period { get; set; }
        [Required]
        public bool IsAvailiable { get; set; }
    }
}
