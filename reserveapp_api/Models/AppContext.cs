﻿using Microsoft.EntityFrameworkCore;

namespace reserveapp_api.Models
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public DbSet<CodesConfirmEmail> EmailCodes { get; set; }
        public DbSet<CodesResetPassword> ResetCodes { get; set; }
        public DbSet<UserToken> Tokens { get; set; }

        public DbSet<User> Users { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Branch> Branches { get; set; }
        public DbSet<Zone> Zones { get; set; }
        public DbSet<Table> Tables { get; set; }

        public DbSet<Tariff> Tariffs { get; set; }
        public DbSet<Bill> Bills { get; set; }

        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<ScheduleDay> ScheduleDays { get; set; }
        public DbSet<DayInterval> DayIntervals { get; set; }

        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<DisabledTime> DisabledTimes { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<CompanyBot> Bots { get; set; }

        public DbSet<EmailDomain> EmailDomains { get; set; }
    }
}