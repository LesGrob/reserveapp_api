﻿using System;
using System.ComponentModel.DataAnnotations;
using reserveapp_api.DTO;

namespace reserveapp_api.Models
{
    public class Bill
    {
        public Bill() { }

        public long Id { get; set; }
        [Required]
        public double Amount { get; set; }
        [Required]
        public int TableCount { get; set; }
        [Required]
        public bool TelegramIsAvailiable { get; set; }
        [Required]
        public double Price { get; set; }
        [Required]
        public double TablePrice { get; set; }
        [Required]
        public double BotPrice { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public virtual Tariff Tariff { get; set; }
        [Required]
        public virtual Branch Branch { get; set; }

        public Bill(Bill data)
        {
            this.Id = data.Id;
            this.Amount = data.Amount;
            this.TableCount = data.TableCount;
            this.TelegramIsAvailiable = data.TelegramIsAvailiable;
            this.Date = data.Date;
            this.Tariff = data.Tariff;
            this.Branch = new Branch(data.Branch);
        }

        public Bill(Bill_UploadDTO data)
        {
            this.Id = data.Id;
            this.Amount = data.Amount;
            this.TableCount = data.TableCount;
            this.TelegramIsAvailiable = data.TelegramIsAvailiable;
            this.Date = data.Date;
            this.Tariff = data.Tariff;
            this.Branch = new Branch(data.Branch);
        }
        
    }
}
