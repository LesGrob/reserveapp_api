﻿using System;
namespace reserveapp_api.Models
{
    public class EmailDomain
    {
        public long Id { get; set; }
        public string Domain { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }

        public EmailDomain() { }
        public EmailDomain(string Domain, string Name, string Url)
        {
            this.Domain = Domain;
            this.Name = Name;
            this.Url = Url;
        }

    }
}
