﻿using System;
using System.ComponentModel.DataAnnotations;
using reserveapp_api.DTO;

namespace reserveapp_api.Models
{
    public class Schedule
    {
        public long Id { get; set; }
        [Required]
        public int ReservationLength { get; set; }

        public Schedule() { ReservationLength = 60; }

        public Schedule(int reservationLength)
        {
            this.ReservationLength = reservationLength;
        }

        public Schedule(Schedule schedule)
        {
            this.Id = schedule.Id;
            this.ReservationLength = schedule.ReservationLength;
        }

        public Schedule(Schedule_DTO schedule)
        {
            this.Id = schedule.Id;
            this.ReservationLength = schedule.ReservationLength;
        }
    }

    public class ScheduleDay
    {
        public long Id { get; set; }
        [Required]
        public virtual Schedule Schedule { get; set; }
        [Required]
        public bool IsActive { get; set; }
        [Required]
        public WeekDay WeekDay { get; set; }

        public ScheduleDay() { IsActive = false; WeekDay = WeekDay.Monday; }

        public ScheduleDay(Schedule schedule, WeekDay weekDay, bool isActive)
        {
            this.Schedule = schedule;
            this.WeekDay = weekDay;
            this.IsActive = isActive;
        }

        public ScheduleDay(ScheduleDay scheduleDay)
        {
            this.Id = scheduleDay.Id;
            this.Schedule = scheduleDay.Schedule;
            this.IsActive = scheduleDay.IsActive;
            this.WeekDay = scheduleDay.WeekDay;
        }
    }

    public class DayInterval
    {
        public long Id { get; set; }
        [Required]
        public virtual ScheduleDay ScheduleDay { get; set; }

        [Required]
        public int Start { get; set; }
        [Required]
        public int End { get; set; }

        public DayInterval() { Start = 0; End = 60; }

        public DayInterval(ScheduleDay scheduleDay, int start, int end)
        {
            this.ScheduleDay = scheduleDay;
            this.Start = start;
            this.End = end;
        }

        public DayInterval(DayInterval dayInterval)
        {
            this.Id = dayInterval.Id;
            this.ScheduleDay = dayInterval.ScheduleDay;
            this.Start = dayInterval.Start;
            this.End = dayInterval.End;
        }
    }
}