using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace reserveapp_api.Models
{
    public class UserDataLogIn
    {
        public UserDataLogIn() { }

        public string Login { get; set; }
        public string Password { get; set; }
    }
}
