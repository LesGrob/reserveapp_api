using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace reserveapp_api.Models
{
    public class UserResetPassword
    {
        public UserResetPassword() { }

        public string Email { get; set; }
        public string Code { get; set; }
        public string Password { get; set; }
    }
}
