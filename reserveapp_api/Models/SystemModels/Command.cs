﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace reserveapp_api.Models.SystemModels
{
    public abstract class Command
    {
        public abstract string Name { get; }

        public abstract Task Execute(Message message, TelegramBotClient client, AppDbContext context);

        public abstract bool Contains(Message message);
    }

    public class StartCommand : Command
    {
        public override string Name => @"/start";

        public override bool Contains(Message message)
        {
            if (message.Type != Telegram.Bot.Types.Enums.MessageType.Text)
                return false;

            return message.Text.Contains(this.Name);
        }

        public override async Task Execute(Message message, TelegramBotClient client, AppDbContext context)
        {
            var chatId = message.Chat.Id;
            await client.SendTextMessageAsync(chatId, "Вас приветстует команда Dengrad! Для начала работы подключите telegram-бота в настройках компании и следуйте инструкциям.", parseMode: Telegram.Bot.Types.Enums.ParseMode.Markdown);
        }
    }

    public class SetConnectionCommand : Command
    {
        public override string Name => @"set_connection: ";

        public override bool Contains(Message message)
        {
            if (message.Type != Telegram.Bot.Types.Enums.MessageType.Text)
                return false;

            return message.Text.Contains(this.Name);
        }

        public override async Task Execute(Message message, TelegramBotClient client, AppDbContext context)
        {
            var chatId = message.Chat.Id;

            string guid = message.Text.Remove(0, Name.Length);

            var bot = context.Bots.FirstOrDefault(x => x.TelegramGuid.ToString() == guid);
            if (bot == null)
            {
                await client.SendTextMessageAsync(chatId, "Введена неправильная команда или код некорректен!", parseMode: Telegram.Bot.Types.Enums.ParseMode.Markdown);
                return;
            }

            var branch = context.Branches.FirstOrDefault(x => x.Id == bot.BranchId);
            if (branch == null)
            {
                await client.SendTextMessageAsync(chatId, "Произошла внутренняя ошибка, попробуйте повторить попытку позже или обратитесь в службу поддержки!", parseMode: Telegram.Bot.Types.Enums.ParseMode.Markdown);
                return;
            }

            bot.TelegramChatId = chatId;
            context.SaveChanges();

            await client.SendTextMessageAsync(chatId, "Бот успешно подключен!", parseMode: Telegram.Bot.Types.Enums.ParseMode.Markdown);
        }
    }
}
