﻿using System;
namespace reserveapp_api.Models
{
    public class CompanyBot
    {
        public CompanyBot(){}

        public long Id { get; set; }
        public long BranchId { get; set; }

        public Guid? TelegramGuid { get; set; }
        public long? TelegramChatId { get; set; }
    }
}
