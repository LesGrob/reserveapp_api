﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace reserveapp_api.Models
{
    public class Order
    {
        public long Id { get; set; }
        [Required]
        public OrderStatus Status { get; set; }

        [Required]
        public string Name { get; set; }
        public string Surname { get; set; }

        public string Phone { get; set; }
        [Required]
        public string Email { get; set; }

        [Required]
        public virtual Branch Branch { get; set; }
        [Required]
        public DateTime Time { get; set; }

        [Required]
        public List<long> Tables { get; set; }
        
        public int PeopleCount { get; set; }

        public Order() { }

        public Order(Order data)
        {
            this.Id = data.Id;
            this.Status = data.Status;
            this.Name = data.Name;
            this.Surname = data.Surname;
            this.Phone = data.Phone;
            this.Email = data.Email;
            this.Branch = data.Branch;
            this.Time = data.Time;
            this.Tables = data.Tables;
            this.PeopleCount = data.PeopleCount;
        }
    }
}
