﻿using System;
using System.ComponentModel.DataAnnotations;

namespace reserveapp_api.Models
{
    public class DisabledTime
    {
        public long Id { get; set; }
        [Required]
        public virtual Branch Branch { get; set; }
        [Required]
        public DateTime TimeStart { get; set; }
        [Required]
        public DateTime TimeEnd { get; set; }

        public DisabledTime() { }

        public DisabledTime(DisabledTime data)
        {
            Id = data.Id;
            Branch = data.Branch;
            TimeStart = data.TimeStart;
            TimeEnd = data.TimeEnd;
        }

        public bool IncludeDate(DateTime date)
        {
            var dateStart = TimeStart;
            var dateEnd = TimeEnd;

            var dS = dateStart.AddSeconds(-dateStart.Second).AddMilliseconds(-dateStart.Millisecond);
            var dE = dateEnd.AddSeconds(-dateEnd.Second).AddMilliseconds(-dateEnd.Millisecond);
            var dC = date.AddSeconds(-date.Second).AddMilliseconds(-date.Millisecond);
            var valF = DateTime.Compare(dS, dC) <= 0;
            var valS = DateTime.Compare(dE, dC) >= 0;
            return valF && valS;
        }
    }
}
