using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace reserveapp_api.Models
{
    public class CodesConfirmEmail
    {
        public CodesConfirmEmail() { }

        public long Id { get; set; }
        public long UserId { get; set; }
        public string Code { get; set; }
    }
}
