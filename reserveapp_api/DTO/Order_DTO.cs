﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using reserveapp_api.Models;

namespace reserveapp_api.DTO
{
    public class Order_DTO
    {
        public long Id { get; set; }

        public OrderStatus Status { get; set; }

        public string Name { get; set; }
        public string Surname { get; set; }

        public string Phone { get; set; }
        public string Email { get; set; }

        public virtual Branch Branch { get; set; }
        public DateTime Time { get; set; }

        public List<Table> Tables { get; set; }

        public Order_DTO(Order order)
        {
            Id = order.Id;
            Status = order.Status;
            Name = order.Name;
            Surname = order.Surname;
            Phone = order.Phone;
            Email = order.Email;
            Branch = order.Branch;
            Time = order.Time;
            Tables = new List<Table>();
        }
    }
}
