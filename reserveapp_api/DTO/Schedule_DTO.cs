﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using reserveapp_api.Models;

namespace reserveapp_api.DTO
{
    public class Schedule_DTO : Schedule
    {
        public List<ScheduleDay_DTO> Days = new List<ScheduleDay_DTO>();

        public Schedule_DTO() { }

        public Schedule_DTO(long Id, List<ScheduleDay_DTO> Days)
        {
            this.Id = Id;
            this.Days = Days;
        }

        public Schedule_DTO(Schedule schedule) : base(schedule)
        {
            this.Days = new List<ScheduleDay_DTO>();
        }

        public Schedule_DTO(Schedule schedule, List<ScheduleDay_DTO> days)
        {
            this.Id = schedule.Id;
            this.ReservationLength = schedule.ReservationLength;
            this.Days = days;
        }



        public bool CheckValid()
        {
            if (ReservationLength < 1) return false;
            if (Days.Count != 7) return false;
            foreach (var day in Days)
                if (!day.CheckValid(ReservationLength))
                    return false;
            return true;
        }

        public bool Contains(DateTime date)
        {
            int dw = date.DayOfWeek.GetHashCode();
            if (dw < 0 || dw > Days.Count) return false;

            ScheduleDay_DTO day = Days.FirstOrDefault(x => x.WeekDay.GetHashCode() == dw);
            return day != null && day.Contains(date, ReservationLength);
        }

        public void PresetSchedele()
        {
            foreach (var day in Days)
                foreach (var interval in day.Intervals)
                    if (interval.End == 0) interval.End = 24 * 60;
        }
    }

    public class ScheduleDay_DTO : ScheduleDay
    {
        //Work with time (ignore date)
        public List<DayInterval_DTO> Intervals = new List<DayInterval_DTO>();


        public ScheduleDay_DTO() { }

        public ScheduleDay_DTO(long Id, bool IsActive, WeekDay WeekDay, List<DayInterval_DTO> Intervals)
        {
            this.Id = Id;
            this.IsActive = IsActive;
            this.WeekDay = WeekDay;
            this.Intervals = Intervals;
        }

        public ScheduleDay_DTO(ScheduleDay scheduleDay) : base(scheduleDay)
        {
            this.Intervals = new List<DayInterval_DTO>();
        }

        public ScheduleDay_DTO(ScheduleDay scheduleDay, List<DayInterval_DTO> intervals) : base(scheduleDay)
        {
            this.Intervals = intervals;
        }


        public bool CheckValid(int reservationLength)
        {
            if (IsActive && Intervals.Count < 1) return false;
            Intervals.Sort((x, y) =>
            {
                if (x.Start < y.Start) return -1;
                if (x.Start > y.Start) return 1;
                return 0;
            });

            if (!Intervals[0].CheckValid(reservationLength)) return false;
            for (int i = 0; i < Intervals.Count - 1; i++)
                if (!Intervals[i + 1].CheckValid(reservationLength) ||
                    Intervals[i + 1].Start < Intervals[i].End)
                    return false;
            return true;
        }

        public bool Contains(DateTime date, int reservationLength)
        {
            foreach (var interval in Intervals)
                if (interval.Contains(date, reservationLength))
                    return true;
            return false;
        }
    }

    public class DayInterval_DTO : DayInterval
    {
        public DayInterval_DTO() { }

        public DayInterval_DTO(long Id, int Start, int End)
        {
            this.Id = Id;
            this.Start = Start;
            this.End = End;
        }

        public DayInterval_DTO(DayInterval interval) : base(interval) { }


        public bool Contains(DateTime date, int reservationLength)
        {
            int minutes = date.Minute + 60 * date.Hour;
            return minutes >= Start && minutes <= (End - reservationLength);
        }

        public bool CheckValid(int reservationLength)
        {
            return (End - reservationLength) >= Start;
        }
    }
}
