﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using reserveapp_api.Models;

namespace reserveapp_api.DTO
{
    public class Zone_DTO : Zone
    {
        public List<Table> Tables = new List<Table>();

        public Zone_DTO() : base() { }

        public Zone_DTO(Zone data) : base(data) { }
    }

    public class Zone_UploadDTO : Zone
    {
        public List<Table_UploadDTO> Tables = new List<Table_UploadDTO>();

        public string ZoneImage { get; set; }
        public string ZoneImageName { get; set; }

        public bool Deleted { get; set; }

        public Zone_UploadDTO() : base() { }

        public Zone_UploadDTO(Zone data) : base(data) { }
    }

    public class Table_UploadDTO : Table
    {
        public bool Deleted { get; set; }

        public Table_UploadDTO() : base() { }

        public Table_UploadDTO(Table data) : base(data) { }
    }
}
