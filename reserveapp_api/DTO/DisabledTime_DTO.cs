﻿using System;
using reserveapp_api.Models;

namespace reserveapp_api.DTO
{
    public class DisabledTime_DTO : DisabledTime
    {
        public int StartMinutes;
        public int EndMinutes;

        public DisabledTime_DTO() { }
        public DisabledTime_DTO(DisabledTime_DTO data): base(data)
        {
            StartMinutes = data.StartMinutes;
            EndMinutes = data.EndMinutes;
        }
        public DisabledTime_DTO(DisabledTime data) : base(data)
        {
            StartMinutes = data.TimeStart.Hour * 60 + data.TimeStart.Minute;
            EndMinutes = data.TimeEnd.Hour * 60 + data.TimeEnd.Minute;
        }

        public DisabledTime ToDisabledTime()
        {
            var dTime = new DisabledTime();
            dTime.Id = Id;
            dTime.Branch = Branch;
            dTime.TimeStart = new DateTime(TimeStart.Year, TimeStart.Month, TimeStart.Day, StartMinutes / 60, StartMinutes % 60, 0);
            dTime.TimeEnd = new DateTime(TimeEnd.Year, TimeEnd.Month, TimeEnd.Day, EndMinutes / 60, EndMinutes % 60, 0);

            return dTime;
        }
    }
}
