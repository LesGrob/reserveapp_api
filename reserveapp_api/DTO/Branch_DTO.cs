﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using reserveapp_api.Models;

namespace reserveapp_api.DTO
{
    public class BranchShort_DTO : Branch
    {
        public BranchStatus Status = BranchStatus.NotPaid;

        public BranchShort_DTO() : base() { }
        public BranchShort_DTO(Branch data) : base(data) { }
    }

    public class Branch_DTO : Branch
    {
        public new Schedule_DTO Schedule;

        public List<Zone_DTO> Zones = new List<Zone_DTO>();
        public List<Zone_DTO> NewZones = new List<Zone_DTO>();

        public BranchStatus Status = BranchStatus.NotPaid;
        public Bill Bill;

        public CompanyBot Bot;

        public Branch_DTO() : base() { }
        public Branch_DTO(Branch data) : base(data)
        {
            Schedule = data.Schedule == null ? null : new Schedule_DTO(data.Schedule);
        }
    }

    public class Branch_UploadDTO : Branch
    {
        public List<Zone_UploadDTO> Zones = new List<Zone_UploadDTO>();
        public List<Zone_UploadDTO> NewZones = new List<Zone_UploadDTO>();

        public string BranchImage { get; set; }
        public string BranchImageName { get; set; }

        public new Schedule_DTO Schedule { get; set; }

        public Branch_UploadDTO() { Guid = Guid.NewGuid(); }

        public Branch_UploadDTO(Branch data): base(data)
        {
            Schedule = new Schedule_DTO(data.Schedule);
        }
    }
}
