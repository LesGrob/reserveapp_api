using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace reserveapp_api.DTO
{
    public class UserChangePassword_DTO
    {
        public string Email { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
