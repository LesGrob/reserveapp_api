﻿using System;
using System.Collections.Generic;
using reserveapp_api.Models;

namespace reserveapp_api.DTO.ReservDTO
{
    public class Order_ReservDTO: Order
    {
        public int TimeMinutes;

        public Order_ReservDTO() { }

        public Order_ReservDTO(Order_ReservDTO data) : base(data) {
            TimeMinutes = data.TimeMinutes;
        }

        public Order_ReservDTO(Order data) : base(data) {
            TimeMinutes = data.Time.Hour * 60 + data.Time.Minute;
        }

        public Order ToOrder()
        {
            var order = new Order();
            order.Id = Id;
            order.Status = Status;
            order.Name = Name;
            order.Surname = Surname;
            order.Phone = Phone;
            order.Email = Email;
            order.Branch = Branch;
            order.Time = new DateTime(Time.Year, Time.Month, Time.Day, TimeMinutes / 60, TimeMinutes % 60, 0);
            order.Tables = Tables;
            order.PeopleCount = PeopleCount;

            return order;
        }
    }
}
