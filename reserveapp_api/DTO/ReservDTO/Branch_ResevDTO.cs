﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using reserveapp_api.Models;

namespace reserveapp_api.DTO
{
    public class Branch_ReservDTO
    {
        public long Id { get; set; }
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public string Adress { get; set; }
        public string Phone { get; set; }
        public string ImagePath { get; set; }
        public Schedule_DTO Schedule { get; set; }
        public DateTime Time { get; set; }

        public List<Zone_ReservDTO> Zones = new List<Zone_ReservDTO>();

        public Branch_ReservDTO() { Guid = Guid.NewGuid(); }

        public Branch_ReservDTO(Branch data)
        {
            Id = data.Id;
            Guid = data.Guid;
            Name = data.Name;
            Adress = data.Adress;
            Phone = data.Phone;
            ImagePath = data.ImagePath;
            Schedule = new Schedule_DTO(data.Schedule);
        }
    }
}
