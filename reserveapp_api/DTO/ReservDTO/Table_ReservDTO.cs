﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using reserveapp_api.Models;

namespace reserveapp_api.DTO
{
    public class Table_ReservDTO : Table
    {
        public bool Availiable = false;

        public Table_ReservDTO(Table data) : base(data) { }
    }
}