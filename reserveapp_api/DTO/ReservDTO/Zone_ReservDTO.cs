﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using reserveapp_api.Models;

namespace reserveapp_api.DTO
{
    public class Zone_ReservDTO : Zone
    {
        public List<Table_ReservDTO> Tables = new List<Table_ReservDTO>();

        public Zone_ReservDTO(Zone floor) : base(floor) { }
    }
}
