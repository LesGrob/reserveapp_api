using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using reserveapp_api.Models;

namespace reserveapp_api.DTO
{
    public class User_DTO
    {
        public long Id { get; set; }
        public string Email { get; set; }
        public User_DTO(User user)
        {
            Id = user.Id;
            Email = user.Email;
        }
    }
}
