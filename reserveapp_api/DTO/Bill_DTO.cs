﻿using System;
using reserveapp_api.Models;

namespace reserveapp_api.DTO
{
    public class Bill_DTO : Bill
    {
        public new Branch_DTO Branch { get; set; }
        public BranchStatus Status = BranchStatus.NotPaid;

        public Bill_DTO() : base() { }
        public Bill_DTO(Bill data) : base(data) {
            Branch = new Branch_DTO(data.Branch);
        }
        public Bill_DTO(Bill_DTO data) : base(data)
        {
            Branch = data.Branch;
        }

        public Bill_DTO(Bill_UploadDTO data) : base(data)
        {
            Branch = new Branch_DTO(data.Branch);
        }
    }

    public class Bill_UploadDTO : Bill
    {
        public new Branch_UploadDTO Branch { get; set; }

        public Bill_UploadDTO() : base() { }
        public Bill_UploadDTO(Bill data) : base(data)
        {
            Branch = new Branch_UploadDTO(data.Branch);
        }
        public Bill_UploadDTO(Bill_UploadDTO data) : base(data)
        {
            Branch = data.Branch;
        }
    }
}
