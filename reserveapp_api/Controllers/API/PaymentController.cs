﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using reserveapp_api.DTO;
using reserveapp_api.Models;
using reserveapp_api.Services;

namespace reserveapp_api.Controllers.API
{

    [Authorize(Policy = "WhiteTokensOnly")]
    [Route("api/[controller]/[action]")]
    public class PaymentController : Controller
    {
        private AppDbContext context;
        public PaymentController(AppDbContext dbContext)
        {
            context = dbContext;
        }

        [HttpPost]
        public ApiResponse Pay([FromBody] Bill_UploadDTO bill)
        {
            long userId = TokensServices.FindUserIdInToken(context, Request);
            User user = context.Users.Include(x => x.Company).FirstOrDefault(x => x.Id == userId);
            if (user == null || user.Company == null) return new ApiResponse(500, "Ошибка данных.");
            if (!user.Company.IsConfirmed) return new ApiResponse(500, "Ваша компания не подтверждена!");

            Branch branch = context.Branches.FirstOrDefault(x => x.Company.Id == user.Company.Id && x.Id == bill.Branch.Id);
            if (branch == null) return new ApiResponse(500, "Филиал не найден.");
            if (branch.Company.Id != user.Company.Id) return new ApiResponse(500, "Ошибка доступа!");

            Tariff tariff = context.Tariffs.FirstOrDefault(x => x.Id == bill.Tariff.Id);
            if (tariff == null) return new ApiResponse(500, "Тариф не найден.");


            //Сохраняем плагин из newZones, после чего работаем именно с ним
            var branchController = new BranchService(context, user);
            ApiResponse result = branchController.Branch(bill.Branch);
            if (result.Code != 200) return result;

            var newzones = context.Zones.Include(x => x.Branch).Where(x => x.Branch.Id == branch.Id && x.Temporary).ToList();

            //Если ничего не изменилось, то проверяем был ли оплачен до этого
            //Если что-то поменялось, то пересчитываем бабки (используем Score) и удаляем NewZones
            //После всего необходимо отменить все предыдущие записи.
            Bill oldBill = context.Bills.OrderBy(x => x.Id).Include(x => x.Branch).Include(x => x.Tariff).LastOrDefault(x => x.Branch.Id == branch.Id);

            if (newzones == null || newzones.Count < 1)//Если ничего не поменялось
            {
                if (oldBill != null && oldBill.Date.AddMonths(oldBill.Tariff.Period) > DateTime.Now)
                    return new ApiResponse(500, "Уже оплачено.");
                //В этом случае: ничего не поменялось и тариф был уже оплачен. Предупреждаем об этом человека.
            }
            else
            {//Если что-то поменялось, то пересчитываем деньги, проводим оплату и перезаписываем newzones в zones
                if (newzones == null || newzones.Count < 1) return new ApiResponse(500, "Невозможно оплатить пустой плагин!.");

                var oldzones = context.Zones.Include(x => x.Branch).Where(x => x.Branch.Id == branch.Id && !x.Temporary);
                context.Zones.RemoveRange(oldzones);

                foreach (var z in newzones) z.Temporary = false;
            }

            var zones = context.Zones.Include(x => x.Branch).Where(x => x.Branch.Id == branch.Id && x.Temporary == false).Select(x => x.Id);
            bill.TableCount = context.Tables.Include(x => x.Zone).Count(x => zones.Any(y => y == x.Zone.Id));
            bill.TelegramIsAvailiable = branch.TelegramIsAvailiable;
            bill.Price = tariff.Price;
            bill.BotPrice = tariff.BotPrice;
            bill.TablePrice = tariff.TablePrice;
            bill.Amount = bill.TableCount * tariff.TablePrice + tariff.Price + (bill.TelegramIsAvailiable ? tariff.BotPrice : 0);
            bill.Date = DateTime.Now;

            if (bill.Amount <= 0 || bill.TableCount <= 0) return new ApiResponse(500, "Некорректные данные.");


            if (newzones != null && newzones.Count > 1)
                //Отправить предупреждение для всех забронировавших пользователей!!!
                RemoveOrdersAsync(branch.Id);

            //if (!branch.Tester)
            //{
            //Здесь происходит оплата через подключенный сервис
            //}

            Bill b = new Bill(bill);

            context.Bills.Add(b);
            context.SaveChanges();

            return branchController.Branch(branch.Id);
        }

        [HttpPost]
        public ApiResponse Prolong([FromBody] Bill_UploadDTO bill)
        {
            long userId = TokensServices.FindUserIdInToken(context, Request);
            User user = context.Users.Include(x => x.Company).FirstOrDefault(x => x.Id == userId);
            if (user == null || user.Company == null) return new ApiResponse(500, "Ошибка данных.");
            if (!user.Company.IsConfirmed) return new ApiResponse(500, "Ваша компания не подтверждена!");

            Branch branch = context.Branches.FirstOrDefault(x => x.Company.Id == user.Company.Id && x.Id == bill.Branch.Id);
            if (branch == null) return new ApiResponse(500, "Филиал не найден.");
            if (branch.Company.Id != user.Company.Id) return new ApiResponse(500, "Ошибка доступа!");

            Tariff tariff = context.Tariffs.FirstOrDefault(x => x.Id == bill.Tariff.Id);
            if (tariff == null) return new ApiResponse(500, "Тариф не найден.");

            Bill oldBill = context.Bills.OrderBy(x => x.Id).Include(x => x.Branch).Include(x => x.Tariff).LastOrDefault(x => x.Branch.Id == branch.Id);
            if (oldBill == null) return new ApiResponse(500, "Невозможно продлить тариф, так как до этого он ни разу не был оплачен.");

            var zones = context.Zones.Include(x => x.Branch).Where(x => x.Branch.Id == branch.Id && x.Temporary == false).Select(x => x.Id);
            bill.TableCount = context.Tables.Include(x => x.Zone).Count(x => zones.Any(y => y == x.Zone.Id));
            bill.TelegramIsAvailiable = oldBill.TelegramIsAvailiable;
            bill.Price = tariff.Price;
            bill.BotPrice = tariff.BotPrice;
            bill.TablePrice = tariff.TablePrice;
            bill.Amount = bill.TableCount * tariff.TablePrice + tariff.Price + (bill.TelegramIsAvailiable ? tariff.BotPrice : 0);

            bill.Date = DateTime.Now;

            if (bill.Amount <= 0 || bill.TableCount <= 0) return new ApiResponse(500, "Некорректные данные.");

            var branchController = new BranchService(context, user);
            //if (!branch.Tester)
            //{
            //Здесь происходит оплата через подключенный сервис
            //}

            Bill b = new Bill(bill);

            context.Bills.Add(b);
            context.SaveChanges();

            return branchController.Branch(branch.Id);
        }


        private async Task RemoveOrdersAsync(long branchId)
        {
            Branch branch = context.Branches.FirstOrDefault(x => x.Id == branchId);
            if (branch == null) return;

            var orders = context.Orders.Include(x => x.Branch).Where(x => x.Branch.Id == branchId);
            foreach (var order in orders)
            {
                context.Orders.Remove(order);
                string message = $"Ваш заказ #{order.Id} в '{branch.Name}' на" +
                    $" {order.Time.Day}.{order.Time.Month}.{order.Time.Year}" +
                    $" {order.Time.Hour}:{order.Time.Minute}" +
                    $" был отменен в связи с изменениями в заведении!" +
                    $"<br />Пожалуйста, забронируйте столик еще раз!";
                EmailService emailService = new EmailService();
                emailService.SendEmailAsync(order.Email, $"Бронирование в {branch.Name}", EmailService.EmailHeaderedText(message));
            }
            context.SaveChanges();
        }
    }
}