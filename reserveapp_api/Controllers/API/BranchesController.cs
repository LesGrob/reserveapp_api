﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using reserveapp_api.DTO;
using reserveapp_api.Models;
using reserveapp_api.Services;

namespace reserveapp_api.Controllers.API
{

    [Authorize(Policy = "WhiteTokensOnly")]
    [Route("api/[controller]/[action]")]
    public class BranchesController : Controller
    {
        private AppDbContext context;
        public BranchesController(AppDbContext dbContext)
        {
            context = dbContext;
        }

        [HttpGet]
        public ApiResponse Tariffs()
        {
            long userId = TokensServices.FindUserIdInToken(context, Request);
            User user = context.Users.Include(x => x.Company).FirstOrDefault(x => x.Id == userId);
            if (user == null || user.Company == null) return new ApiResponse(500, "Ошибка данных.");

            return new ApiResponse(context.Tariffs.Where(x => x.IsAvailiable).ToList());
        }

        [HttpGet]
        public ApiResponse Branches()
        {
            long userId = TokensServices.FindUserIdInToken(context, Request);
            User user = context.Users.Include(x => x.Company).FirstOrDefault(x => x.Id == userId);
            if (user == null || user.Company == null) return new ApiResponse(500, "Ошибка данных.");
            List<BranchShort_DTO> branches = context.Branches.Where(x => x.Company.Id == user.Company.Id)
                                                        .Select(x => new BranchShort_DTO(x)).ToList();
            foreach (var branch in branches)
            {
                Bill bill = context.Bills.Include(x => x.Branch).Include(x => x.Tariff).FirstOrDefault(x => x.Branch.Id == branch.Id);
                if (bill == null) branch.Status = BranchStatus.NotPaid;
                else branch.Status = bill.Date.AddMonths(bill.Tariff.Period) > DateTime.Now ? BranchStatus.Paid : BranchStatus.NotPaid;
            }
            return new ApiResponse(branches);
        }

        [HttpGet("{branchId}")]
        public ApiResponse Branch(int branchId)
        {
            long userId = TokensServices.FindUserIdInToken(context, Request);
            User user = context.Users.Include(x => x.Company).FirstOrDefault(x => x.Id == userId);
            if (user == null || user.Company == null) return new ApiResponse(500, "Ошибка данных.");

            var service = new BranchService(context,user);
            return service.Branch(branchId);
        }

        [HttpPost("{branchId}")]
        public ApiResponse RemoveBranch(int branchId)
        {
            long userId = TokensServices.FindUserIdInToken(context, Request);
            User user = context.Users.Include(x => x.Company).FirstOrDefault(x => x.Id == userId);
            if (user == null || user.Company == null) return new ApiResponse(500, "Ошибка данных.");
            if (!user.Company.IsConfirmed) return new ApiResponse(500, "Ваша компания не подтверждена!");

            var service = new BranchService(context, user);
            return service.RemoveBranch(branchId);
        }

        [HttpPost]
        public ApiResponse Branch([FromBody] Branch_UploadDTO newBranch)
        {
            if (newBranch == null) return new ApiResponse(500, "Пустые данные.");

            long userId = TokensServices.FindUserIdInToken(context, Request);
            User user = context.Users.Include(x => x.Company).FirstOrDefault(x => x.Id == userId);
            if (user == null || user.Company == null) return new ApiResponse(500, "Ошибка данных.");
            if (!user.Company.IsConfirmed) return new ApiResponse(500, "Ваша компания не подтверждена!");

            var service = new BranchService(context, user);
            return service.Branch(newBranch);
        }
    }
}
