﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using reserveapp_api.DTO;
using reserveapp_api.DTO.ReservDTO;
using reserveapp_api.Models;
using reserveapp_api.Models.SystemModels;
using reserveapp_api.Services;
using Telegram.Bot;

namespace reserveapp_api.Controllers.API
{
    [Route("api/[controller]/[action]")]
    public class ClientController : Controller
    {
        private AppDbContext context;
        public ClientController(AppDbContext dbContext)
        {
            context = dbContext;
        }

        [HttpGet("{branchId}/{day}/{timeMinutes}")]
        public ApiResponse Branch(int branchId, DateTime day, int timeMinutes)
        {
            DateTime time = new DateTime(day.Year, day.Month, day.Day, timeMinutes / 60, timeMinutes % 60, 0);
            Branch_ReservDTO branch = new Branch_ReservDTO(context.Branches.Include(x => x.Tariff).Include(x => x.Schedule).FirstOrDefault(x => x.Id == branchId));
            if (branch == null) return new ApiResponse(500, "Филиал не найден.");
            branch.Schedule = Schedule(branch.Schedule);
            try
            {
                branch.Time = time;

                bool timeValid = time > DateTime.Now;
                bool scheduleAvailiable = branch.Schedule.Contains(time);
                bool disabledTimeAvailiable = AvailiableByDisabledTime(time, branch.Id);
                var orders = context.Orders.Include(x => x.Branch).Include(x => x.Branch.Schedule)
                            .Where(x => x.Branch.Id == branchId && x.Time.AddMinutes(-x.Branch.Schedule.ReservationLength) <= time && x.Time.AddMinutes(x.Branch.Schedule.ReservationLength) >= time).ToList();

                foreach (var floor in context.Zones.Include(x => x.Branch).Where(x => x.Branch.Id == branch.Id && x.Temporary == false))
                {
                    Zone_ReservDTO f = new Zone_ReservDTO(floor);
                    foreach (var table in context.Tables.Include(x => x.Zone).Where(x => x.Zone.Id == floor.Id))
                    {
                        Table_ReservDTO t = new Table_ReservDTO(table);
                        t.Availiable = timeValid && scheduleAvailiable && disabledTimeAvailiable && !orders.Any(x => x.Tables.Any(y => y == table.Id));
                        f.Tables.Add(t);
                    }
                    branch.Zones.Add(f);
                }
                return new ApiResponse(branch);
            }
            catch (Exception ex)
            {
                return new ApiResponse(500, "Ошибка сервера.");
            }
        }

        [HttpPost]
        public ApiResponse Order([FromBody] Order_ReservDTO order)
        {
            if (order == null) return new ApiResponse(500, "Пустые данные.");

            Branch branch = context.Branches.Include(x => x.Tariff).FirstOrDefault(x => x.Id == order.Branch.Id);
            if (branch == null) return new ApiResponse(500, "Филиал не найден.");

            //Проверка на то, оплачен ли плагин
            Bill oldBill = context.Bills.OrderBy(x => x.Id).Include(x => x.Branch).Include(x => x.Tariff).LastOrDefault(x => x.Branch.Id == branch.Id);
            if (oldBill == null || oldBill.Date.AddMonths(oldBill.Tariff.Period) <= DateTime.Now)
                return new ApiResponse(500, "Запись в заведении недоступна!");

            if (!Regex.IsMatch(order.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
                return new ApiResponse(500, "Неверный e-mail.");

            if (order.Time < DateTime.Now) return new ApiResponse(500, "Неверное время бронирования.");

            var tables = context.Tables.Include(x => x.Zone.Branch).Where(x => x.Zone.Branch.Id == branch.Id).ToList();
            foreach (var table in order.Tables)
                if (!tables.Any(x => x.Id == table)) return new ApiResponse(500, "Неверный идентификатор стола.");

            var orders = context.Orders.Include(x => x.Branch).Include(x => x.Branch.Schedule).Where(x => x.Branch.Id == branch.Id && x.Time <= order.Time && x.Time.AddMinutes(x.Branch.Schedule.ReservationLength) >= order.Time).ToList();
            foreach (var table in order.Tables)
                if (orders.Any(x => x.Tables.Any(y => y == table))) return new ApiResponse(500, "На это время уже есть заказ.");
            try
            {
                Order newOrder = new Order();
                newOrder.Id = 0;
                newOrder.Status = order.Status;
                newOrder.Name = order.Name;
                newOrder.Surname = order.Surname;
                newOrder.Phone = order.Phone;
                newOrder.Email = order.Email;
                newOrder.Branch = branch;
                newOrder.Time = new DateTime(order.Time.Year, order.Time.Month, order.Time.Day, order.TimeMinutes / 60, order.TimeMinutes % 60, 0);
                newOrder.Tables = new List<long>();
                newOrder.PeopleCount = order.PeopleCount;

                context.Orders.Add(newOrder);

                newOrder.Tables = tables.Select(x => x.Id).ToList();

                context.SaveChanges();

                // отправка сообщения на почту
                // пока без подтверждения
                string message = $"Ваш заказ #{order.Id} в '{branch.Name}' был зарегестрирован!" +
                    $"  Дата: {order.Time.Day}.{order.Time.Month}.{order.Time.Year}" +
                    $"<br />Время: {order.Time.Hour}:{order.Time.Minute}";
                EmailService emailService = new EmailService();
                emailService.SendEmailAsync(order.Email, $"Бронирование в {branch.Name}", EmailService.EmailHeaderedText(message));

                //TelegramBot отправка сообщения в компанию
                string botMessage = $"Заказ #{order.Id} в '{branch.Name}' был зарегестрирован!\n" +
                    $"Имя: {newOrder.Name} {newOrder.Surname}.\n" +
                    $"Телефон: {newOrder.Phone}. Почта: {newOrder.Email}\n" +
                    $"Дата: {order.Time.Day}.{order.Time.Month}.{order.Time.Year}\n" +
                    $"Время: {order.Time.ToString("HH:mm")}.";
                var botController = new BotController(context);
                botController.SendTelegramMessage(botMessage, branch);

                return new ApiResponse(newOrder);
            }
            catch (Exception ex)
            {
                return new ApiResponse(500, "Ошибка сервера.");
            }
        }


        // custom funcs
        private bool AvailiableByDisabledTime(DateTime date, long branchId)
        {
            return !context.DisabledTimes.Any(x => x.Branch.Id == branchId && x.IncludeDate(date));
        }

        private Schedule_DTO Schedule(Schedule_DTO schedule)
        {
            schedule.Days = context.ScheduleDays.Where(x => x.Schedule.Id == schedule.Id)
                                                .Select(x => new ScheduleDay_DTO(x)).ToList();
            foreach (var day in schedule.Days)
                day.Intervals = context.DayIntervals.Where(x => x.ScheduleDay.Id == day.Id)
                                                    .Select(x => new DayInterval_DTO(x)).ToList();
            return schedule;
        }

    }
}