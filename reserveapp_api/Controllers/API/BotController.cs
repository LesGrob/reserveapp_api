﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using reserveapp_api.Models;
using reserveapp_api.Models.SystemModels;
using Telegram.Bot.Types;

namespace reserveapp_api.Controllers.API
{
    [Route("api/bot/update")]
    public class BotController : Controller
    {
        private AppDbContext context;
        public BotController(AppDbContext dbContext)
        {
            context = dbContext;
        }

        [HttpPost]
        public async Task<OkResult> Post([FromBody]Update update)
        {
            if (update == null) return Ok();

            var commands = Bot.Commands;
            var message = update.Message;
            var botClient = await Bot.GetBotClientAsync();

            foreach (var command in commands)
            {
                if (command.Contains(message))
                {
                    await command.Execute(message, botClient, context);
                    break;
                }
            }
            return Ok();
        }

        public void RemoveTelegramBot(Branch branch)
        {
            var oldBot = context.Bots.FirstOrDefault(x => x.BranchId == branch.Id);
            if (oldBot != null)
                context.Bots.Remove(oldBot);
        }

        public Guid AddTelegramBot(Branch branch)
        {
            var oldBot = context.Bots.FirstOrDefault(x => x.BranchId == branch.Id);
            if (oldBot != null)
            {
                return oldBot.TelegramGuid.Value;
            }

            var newBot = new CompanyBot();
            newBot.BranchId = branch.Id;
            newBot.TelegramGuid = Guid.NewGuid();
            context.Bots.Add(newBot);
            context.SaveChanges();

            return newBot.TelegramGuid.Value;
        }

        public async Task<OkResult> SendTelegramMessage(string message, Branch branch)
        {
            Bill bill = context.Bills.OrderBy(x => x.Id).Include(x => x.Branch).Include(x => x.Tariff).LastOrDefault(x => x.Branch.Id == branch.Id);
            if (bill == null || bill.Date.AddMonths(bill.Tariff.Period) < DateTime.Now || !bill.TelegramIsAvailiable)
            {
                Console.Write("Telegram messaging unavailable");
                return Ok();
            }

            var bot = context.Bots.FirstOrDefault(x => x.BranchId == branch.Id);
            if (bot == null || bot.TelegramChatId == null)
            {
                Console.Write("Bot is null or no chat Id");
                return Ok();
            }

            var botClient = await Bot.GetBotClientAsync();
            await botClient.SendTextMessageAsync(bot.TelegramChatId, message);
            return Ok();
        }
    }
}
