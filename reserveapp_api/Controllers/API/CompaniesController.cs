﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using reserveapp_api.Models;

namespace reserveapp_api.Controllers.API
{

    [Authorize(Policy = "WhiteTokensOnly")]
    [Route("api/[controller]/[action]")]
    public class CompaniesController : Controller
    {
        private AppDbContext context;
        public CompaniesController(AppDbContext dbContext)
        {
            context = dbContext;
        }

        [HttpGet]
        public ApiResponse Company()
        {
            long userId = TokensServices.FindUserIdInToken(context, Request);
            User user = context.Users.Include(x => x.Company).FirstOrDefault(x => x.Id == userId);
            if (user != null && user.Company != null)
                return new ApiResponse(user.Company);
            return new ApiResponse(500, "Ошибка данных.");
        }

        [HttpPost]
        public ApiResponse ChangeCompany([FromBody] Company newCompany)
        {
            long userId = TokensServices.FindUserIdInToken(context, Request);
            User user = context.Users.Include(x => x.Company).FirstOrDefault(x => x.Id == userId);
            if (user == null || user.Company == null)
                return new ApiResponse(500, "Ошибка данных.");
            Company company = context.Companies.FirstOrDefault(x => x.Id == user.Company.Id);
            if (user.Company == null)
                return new ApiResponse(500, "Ошибка данных.");

            if (!Сheck_INN(newCompany.INN)) { return new ApiResponse(500, "Неверный ИНН."); }
            if (!Сheck_OGRN(newCompany.OGRN)) { return new ApiResponse(500, "Неверный ОГРН."); }

            company.Name = newCompany.Name;
            company.INN = newCompany.INN;
            company.IsIP = newCompany.IsIP;
            company.OGRN = newCompany.OGRN;
            company.IsConfirmed = true;

            context.SaveChanges();
            return new ApiResponse(company);
        }

        private bool Сheck_INN(string INNstring)
        {
            // является ли вообще числом
            try { Int64.Parse(INNstring); } catch { return false; }

            // проверка на 10 и 12 цифр
            if (INNstring.Length != 10 && INNstring.Length != 12) { return false; }

            // проверка по контрольным цифрам
            if (INNstring.Length == 10) // для юридического лица
            {
                int dgt10 = 0;
                try
                {
                    dgt10 = (((2 * Int32.Parse(INNstring.Substring(0, 1))
                        + 4 * Int32.Parse(INNstring.Substring(1, 1))
                        + 10 * Int32.Parse(INNstring.Substring(2, 1))
                        + 3 * Int32.Parse(INNstring.Substring(3, 1))
                        + 5 * Int32.Parse(INNstring.Substring(4, 1))
                        + 9 * Int32.Parse(INNstring.Substring(5, 1))
                        + 4 * Int32.Parse(INNstring.Substring(6, 1))
                        + 6 * Int32.Parse(INNstring.Substring(7, 1))
                        + 8 * Int32.Parse(INNstring.Substring(8, 1))) % 11) % 10);
                }
                catch { return false; }

                if (Int32.Parse(INNstring.Substring(9, 1)) == dgt10) { return true; }
                else { return false; }
            }
            else // для физического лица
            {
                int dgt11 = 0, dgt12 = 0;
                try
                {
                    dgt11 = (((
                        7 * Int32.Parse(INNstring.Substring(0, 1))
                        + 2 * Int32.Parse(INNstring.Substring(1, 1))
                        + 4 * Int32.Parse(INNstring.Substring(2, 1))
                        + 10 * Int32.Parse(INNstring.Substring(3, 1))
                        + 3 * Int32.Parse(INNstring.Substring(4, 1))
                        + 5 * Int32.Parse(INNstring.Substring(5, 1))
                        + 9 * Int32.Parse(INNstring.Substring(6, 1))
                        + 4 * Int32.Parse(INNstring.Substring(7, 1))
                        + 6 * Int32.Parse(INNstring.Substring(8, 1))
                        + 8 * Int32.Parse(INNstring.Substring(9, 1))) % 11) % 10);
                    dgt12 = (((
                        3 * Int32.Parse(INNstring.Substring(0, 1))
                        + 7 * Int32.Parse(INNstring.Substring(1, 1))
                        + 2 * Int32.Parse(INNstring.Substring(2, 1))
                        + 4 * Int32.Parse(INNstring.Substring(3, 1))
                        + 10 * Int32.Parse(INNstring.Substring(4, 1))
                        + 3 * Int32.Parse(INNstring.Substring(5, 1))
                        + 5 * Int32.Parse(INNstring.Substring(6, 1))
                        + 9 * Int32.Parse(INNstring.Substring(7, 1))
                        + 4 * Int32.Parse(INNstring.Substring(8, 1))
                        + 6 * Int32.Parse(INNstring.Substring(9, 1))
                        + 8 * Int32.Parse(INNstring.Substring(10, 1))) % 11) % 10);
                }
                catch { return false; }
                if (Int32.Parse(INNstring.Substring(10, 1)) == dgt11
                    && Int32.Parse(INNstring.Substring(11, 1)) == dgt12) { return true; }
                else { return false; }
            }
        }

        public bool Сheck_OGRN(string OGRNstring)
        {
            if (string.IsNullOrEmpty(OGRNstring)) return true;
            // является ли вообще числом
            long number = 0;
            try { number = Int64.Parse(OGRNstring); }
            catch { return false; }

            // проверка на 13 и 15 цифр
            if (OGRNstring.Length != 13 && OGRNstring.Length != 15) { return false; }

            // проверка по контрольным цифрам
            if (OGRNstring.Length == 13) // для юридического лица
            {
                // остаток от деления
                int num12 = (int)Math.Floor(((double)number / 10) % 11);
                int dgt13 = -1;
                // если остаток равен 10, то берём 0, если нет, то берём его самого
                if (num12 == 10) { dgt13 = 0; } else { dgt13 = num12; }
                // ну и теперь сравниваем с контрольной цифрой
                if (Int32.Parse(OGRNstring.Substring(12, 1)) == dgt13) { return true; }
                else { return false; }
            }
            else // для индивидуального предпринимателя
            {
                // остаток от деления
                int num14 = (int)Math.Floor(((double)number / 10) % 13);
                int dgt15 = num14 % 10;
                // ну и теперь сравниваем с контрольной цифрой
                if (Int32.Parse(OGRNstring.Substring(14, 1)) == dgt15) { return true; }
                else { return false; }
            }
        }
    }
}