using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using reserveapp_api.Models;
using reserveapp_api.DTO;
using System.Text.RegularExpressions;
using System.Net.Http;
using System.Net;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using System.Security.Cryptography;
using Microsoft.EntityFrameworkCore;
using reserveapp_api.Services;
using System.Net.Mail;

namespace reserveapp_api.Controllers.API
{
    [Authorize(Policy = "WhiteTokensOnly")]
    [Route("api/[controller]/[action]")]
    public class UsersController : Controller
    {
        private AppDbContext context;

        public UsersController(AppDbContext dbContext)
        {
            context = dbContext;
        }

        private bool EmailIsValid(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        //Registration
        [AllowAnonymous, HttpPost]
        [ProducesResponseType(typeof(User_DTO), 200)]
        public ApiResponse User([FromBody] User user)
        {
            user.Email = user.Email.ToLower();

            if (context.Users.Any(x => x.Email.ToLower() == user.Email.ToLower()))
            {
                return new ApiResponse(500, "Пользователь с такой почтой уже существует.");
            }

            if (!EmailIsValid(user.Email)) return new ApiResponse(500, "Неверный адрес электронной почты.");

            Company company = new Company();
            company.Name = "Название компании";
            company.INN = "0";
            company.IsConfirmed = false;
            company.IsIP = false;

            user.Salt = Guid.NewGuid().ToString();
            user.Password = ComputePasswordHash(user.Password, user.Salt);
            user.Guid = Guid.NewGuid();
            user.IsEmailConfirmed = false;
            user.Company = company;

            context.Companies.Add(company);
            context.Users.Add(user);
            context.SaveChanges();

            //SendLetter
            string code = Guid.NewGuid().ToString();
            var callbackUrl = Url.Action("ConfirmEmail", "Users", new { userId = user.Id, code = code },
                protocol: HttpContext.Request.Scheme);
            EmailService emailService = new EmailService();

            //WriteInTableCode
            CodesConfirmEmail RecordCode = new CodesConfirmEmail();
            context.EmailCodes.Add(new CodesConfirmEmail { UserId = user.Id, Code = code });
            context.SaveChanges();

            //MaskEmail
            string masked = user.Email.Split("@")[0][0] + "***" + user.Email.Split("@")[0].Last() + "@" +
                            user.Email.Split("@")[1][0] + "***" + "." + user.Email.Split(".").Last();

            emailService.SendEmailAsync(user.Email, "Пожалуйста, подтвердите E-mail", EmailService.EmailRegistrationText(callbackUrl));

            var emailDomain = context.EmailDomains.FirstOrDefault(x => user.Email.Contains(x.Domain));
            return new ApiResponse(emailDomain);
        }

        [AllowAnonymous, HttpGet]
        public object ConfirmEmail(long userId, string code)
        {
            CodesConfirmEmail Code = context.EmailCodes.Last(x => x.UserId == userId);
            if (Code.Code == code)
            {
                context.Users.Find(userId).IsEmailConfirmed = true;
                context.SaveChanges();
                return Redirect("/LogIn");
            }
            return Json("Error");
        }



        class LoginResponse
        {
            public string Token = "";
            public bool IsConfirmed = false;
        }

        //Log In
        [AllowAnonymous, HttpPost]
        public ApiResponse LogIn([FromBody] UserDataLogIn userData)
        {
            userData.Login = userData.Login.ToLower();
            if (!context.Users.Any(x => x.Email.ToLower() == userData.Login.ToLower()))
                return new ApiResponse(500, "Неверный логин или пароль!");

            //AnyValidation
            userData.Login = Regex.Replace(userData.Login, "^((7|8)+([0-9]){10})$", "+7" + userData.Login.Substring(1));
            string userSalt = context.Users.Where(x => x.Email.ToLower() == userData.Login).First().Salt;
            string hashPassword = ComputePasswordHash(userData.Password, userSalt);

            var user = context.Users.Include(x => x.Company).Where(x => x.Email.ToLower() == userData.Login && x.Password == hashPassword).ToList();
            if (user.Count < 1) return new ApiResponse(500, "Неверный логин или пароль!");


            //password hashing
            User userHashed = user.First();
            if (!userHashed.IsEmailConfirmed) return new ApiResponse(500, "Необходимо подтвердить почту, чтобы войти!");

            var token = TokensServices.CreateToken(userHashed, context);
            Response.StatusCode = (int)HttpStatusCode.OK;
            Response.Cookies.Append("UserId", userHashed.Id.ToString());

            LoginResponse loginResponse = new LoginResponse();
            loginResponse.IsConfirmed = userHashed.Company.IsConfirmed;
            loginResponse.Token = token as string;
            return new ApiResponse(loginResponse);
        }

        //Logout
        [HttpPost]
        public ApiResponse LogOut()
        {
            //Выход пользователя из приложения
            //Удаление токена пользователя из белого листа

            //Представление пользовательского токена
            string token = TokensServices.GetEncodedStringToken(Request);
            //id пользователя
            long userId = TokensServices.FindUserIdInToken(context, Request);
            List<UserToken> tokens = context.Tokens.Where(x => x.user.Id == userId && x.Token == token).ToList();
            if (tokens.Count > 0) context.Tokens.RemoveRange(tokens);
            context.SaveChanges();
            return new ApiResponse("LogOut");
        }


        //Refresh Token on login
        [HttpPost]
        public ApiResponse RefreshToken([FromBody] UserToken oldToken)
        {
            //Подмена токена пользователя - удаление старого и выдача нового
            long userId = TokensServices.FindUserIdInToken(context, Request);
            var token = TokensServices.RefreshToken(context, Request, oldToken);
            return new ApiResponse(token);
        }


        //Reset and change password
        [AllowAnonymous, HttpPost]
        public ApiResponse ResetPassword([FromBody] User ReqUser)
        {
            //Возврат url для сброса пароля
            //Уведомление на Email
            EmailService emailService = new EmailService();
            if (!context.Users.Where(x => x.Email.ToLower() == ReqUser.Email.ToLower()).Any())
                return new ApiResponse(500, "Пользователя с таким Email не существует!");

            User user = context.Users.Where(x => x.Email == ReqUser.Email).First();
            string masked = user.Email.Split("@")[0][0] + "***" + user.Email.Split("@")[0].Last() + "@" + user.Email.Split("@")[1][0] + "***" + "." + user.Email.Split(".").Last();
            string code = Guid.NewGuid().ToString();
            string host = HttpContext.Request.Host.Value;
            string link = $"http://{host}/Auth/ResetPassword";

            var callbackUrl = Url.Action("SaveUserData", "Users", new { link = link }, protocol: HttpContext.Request.Scheme);

            //WriteInTableCode
            context.ResetCodes.Add(new CodesResetPassword { UserId = user.Id, Code = code });
            context.SaveChanges();

            emailService.SendEmailAsync(user.Email, "Сброс пароля на платформе Dengrad", EmailService.EmailResetPasswordText(masked, code, callbackUrl));

            return new ApiResponse("Письмо с инструкцией выслано на почту!");
        }

        [AllowAnonymous, HttpGet]
        public object SaveUserData(string link)
        {
            return Redirect(link);
        }

        [AllowAnonymous, HttpPost]
        public ApiResponse ChangePassword([FromBody] UserResetPassword userData)
        {
            //Завершение сессий пользователя, отбор всех токенов
            List<UserToken> tokens = context.Tokens.Include(x => x.user).Where(x => x.user.Email == userData.Email).ToList();
            if (tokens.Count > 0)
            {
                context.Tokens.RemoveRange(tokens);
                context.SaveChanges();
            }

            User user = context.Users.Where(x => x.Email == userData.Email).First();
            if (context.ResetCodes.Where(x => x.UserId == user.Id && x.Code == userData.Code).Any())
            {
                //Хеширование пароля
                string hashPassword = ComputePasswordHash(userData.Password, user.Salt);
                //user.Password = userData.Password;
                user.Password = hashPassword;
                context.ResetCodes.RemoveRange(context.ResetCodes.Where(x => x.UserId == user.Id && x.Code == userData.Code).First());
                context.SaveChanges();
                var callbackUrl = Url.Action("", "", new { }, protocol: HttpContext.Request.Scheme);
                string link = $"{callbackUrl}LogIn";
                return new ApiResponse("Ваш пароль был успешно изменен.");
            }
            return new ApiResponse(500, "Введены некорректные данные!");
        }

        [HttpPost]
        public ApiResponse ChangingPass([FromBody] UserChangePassword_DTO user_dto)
        {
            Models.User user = context.Users.Find(TokensServices.FindUserIdInToken(context, Request));
            //Если старый пароль совпадает
            if (ComputePasswordHash(user_dto.OldPassword, user.Salt) == user.Password)
            {
                user.Password = ComputePasswordHash(user_dto.NewPassword, user.Salt);
                context.SaveChanges();
                return new ApiResponse("Пароль успешно изменен");
            }
            else
                return new ApiResponse("Старый пароль введен неверно");
        }


        //Change Email
        [HttpPost]
        public ApiResponse ChangeEmail([FromBody] User user)
        {
            User currentUser = context.Users.Find(TokensServices.FindUserIdInToken(context, Request));

            if (currentUser.Id == user.Id)
            {
                if (currentUser.Email == user.Email)
                    return new ApiResponse("Email совпадае с предыдущим!");
                if (context.Users.Any(x => x.Email == currentUser.Email))
                    return new ApiResponse("Введенный Email уже зарегестрирован!");

                string code = Guid.NewGuid().ToString();
                var callbackUrl = Url.Action("ConfirmEmailChange", "Users",
                    new { userId = user.Id, code = code, email = user.Email }, protocol: HttpContext.Request.Scheme);
                EmailService emailService = new EmailService();

                //WriteInTableCode
                context.EmailCodes.Add(new CodesConfirmEmail { UserId = user.Id, Code = code });
                context.SaveChanges();

                string masked = user.Email.Split("@")[0][0] + "***" + user.Email.Split("@")[0].Last() + "@" +
                                user.Email.Split("@")[1][0] + "***" + "." + user.Email.Split(".").Last();

                var message = $"Вы получили данное письмо, потому что пользователь изменил Email на {masked}. " +
                    $"<br />Если это действие было произведено вами, пройдите по ссылке для подтверждения: <a href='{callbackUrl}'>Подтверждаю</a>";
                emailService.SendEmailAsync(user.Email, "Смена почты на платформе Dengrad", EmailService.EmailHeaderedText(message));

                return new ApiResponse("Письмо выслано на почту!");
            }
            else
            {
                return new ApiResponse("Невозможно изменить Email!");
            }
        }

        [AllowAnonymous, HttpGet]
        public object ConfirmEmailChange(long userId, string code, string email)
        {
            CodesConfirmEmail Code = context.EmailCodes.Last(x => x.UserId == userId);
            if (Code.Code == code)
            {
                User currentUser = context.Users.FirstOrDefault(x => x.Id == userId);

                //Завершение сессий пользователя, отбор всех токенов
                List<UserToken> tokens = context.Tokens.Include(x => x.user)
                    .Where(x => x.user.Email == currentUser.Email).ToList();
                if (tokens.Count > 0)
                {
                    context.Tokens.RemoveRange(tokens);
                    context.SaveChanges();
                }

                currentUser.Email = email;
                context.SaveChanges();
                return Redirect("/LogIn");
            }

            return Json("Error");
        }

        private string ComputePasswordHash(string password, string salt)
        {
            SHA512 sha = SHA512.Create();
            //Пароль + соль
            string valToHash = password + salt;
            byte[] bytes = Encoding.Default.GetBytes(valToHash);
            byte[] hashBytes = sha.ComputeHash(bytes);
            string hash = "";
            for (int i = 0; i < hashBytes.Length; i++)
                hash += hashBytes[i].ToString("X");
            return hash;
        }
    }
}
