﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using reserveapp_api.DTO;
using reserveapp_api.Models;

namespace reserveapp_api.Controllers.API
{

    public class TestA
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public List<TestB> ObjList = new List<TestB>();
    }

    public class TestB
    {
        public long Id { get; set; }
        public string Surname { get; set; }
    }

    [Authorize(Policy = "WhiteTokensOnly")]
    [Route("api/[controller]/[action]")]
    public class TestController : Controller
    {
        private AppDbContext context;
        private ErrorStatus ErrorStatus;
        public TestController(AppDbContext dbContext)
        {
            context = dbContext;
            ErrorStatus = new ErrorStatus();
        }

        [HttpPost]
        public object PostData([FromBody] TestA testA)
        {
            string s = testA.Name;
            return Ok();
        }
    }
}