﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using reserveapp_api.DTO;
using reserveapp_api.Models;
using reserveapp_api.Services;

namespace reserveapp_api.Controllers.API
{
    [Route("api/[controller]/[action]")]
    public class CalendarController : Controller
    {
        private AppDbContext context;
        public CalendarController(AppDbContext dbContext)
        {
            context = dbContext;
        }

        [HttpGet("{branchId}/{time}")]
        public ApiResponse Orders(int branchId, DateTime time)
        {
            Branch_ReservDTO branch = new Branch_ReservDTO(context.Branches.Include(x => x.Tariff).Include(x => x.Schedule).Include(x => x.Tariff).FirstOrDefault(x => x.Id == branchId));
            if (branch == null) return new ApiResponse(500, "Филиал не найден.");

            try
            {
                var orders = context.Orders.Include(x => x.Branch).Include(x => x.Branch.Schedule)
                                    .Where(x => x.Branch.Id == branch.Id && x.Time.Year == time.Year && x.Time.Month == time.Month).ToList();
                var orders_dto = new List<Order_DTO>();

                foreach(var order in orders)
                {
                    var tables = context.Tables.Where(x => order.Tables.Contains(x.Id)).ToList();
                    var or = new Order_DTO(order);
                    or.Tables = tables;
                    orders_dto.Add(or);
                }

                return new ApiResponse(orders_dto);
            }
            catch (Exception ex)
            {
                return new ApiResponse(500, "Ошибка сервера.");
            }
        }
    }
}