﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using reserveapp_api.DTO;
using reserveapp_api.Models;

namespace reserveapp_api.Controllers.API
{

    [Authorize(Policy = "WhiteTokensOnly")]
    [Route("api/[controller]/[action]")]
    public class DisabledTimeController : Controller
    {
        private AppDbContext context;
        public DisabledTimeController(AppDbContext dbContext)
        {
            context = dbContext;
        }

        [HttpGet("{branchId}")]
        public ApiResponse DisabledTime(int branchId)
        {
            long userId = TokensServices.FindUserIdInToken(context, Request);
            User user = context.Users.Include(x => x.Company).FirstOrDefault(x => x.Id == userId);
            if (user == null || user.Company == null) return new ApiResponse(500, "Ошибка данных.");

            List<DisabledTime> disabledTimes = context.DisabledTimes.Include(x => x.Branch).Include(x => x.Branch.Company)
                                .Where(x => x.Branch.Id == branchId && x.Branch.Company.Id == user.Company.Id).ToList();
            return new ApiResponse(disabledTimes.Select(x => new DisabledTime_DTO(x)));
        }


        [HttpPost]
        public ApiResponse AddDisabledTime([FromBody] DisabledTime_DTO dTime)
        {
            DisabledTime disabledTime = dTime.ToDisabledTime();
            long userId = TokensServices.FindUserIdInToken(context, Request);
            User user = context.Users.Include(x => x.Company).FirstOrDefault(x => x.Id == userId);
            if (user == null || user.Company == null) return new ApiResponse(500, "Ошибка данных.");

            if (disabledTime.TimeStart >= disabledTime.TimeEnd) return new ApiResponse(500, "Неверное время.");
            if (disabledTime.TimeStart < DateTime.Now) return new ApiResponse(500, "Нельзя добавить данные в прошлое!");

            disabledTime.Branch = context.Branches.FirstOrDefault(x => x.Id == dTime.Branch.Id);
            if (disabledTime.Branch == null) return new ApiResponse(500, "Ошибка данных.");

            context.DisabledTimes.Add(disabledTime);
            context.SaveChanges();

            List<DisabledTime> disabledTimes = context.DisabledTimes.Include(x => x.Branch).Include(x => x.Branch.Company)
                                .Where(x => x.Branch.Id == dTime.Branch.Id && x.Branch.Company.Id == user.Company.Id).ToList();

            return new ApiResponse(disabledTimes.Select(x => new DisabledTime_DTO(x)));
        }

        [HttpPost("{timeId}")]
        public ApiResponse RemoveDisabledTime(int timeId)
        {
            long userId = TokensServices.FindUserIdInToken(context, Request);
            User user = context.Users.Include(x => x.Company).FirstOrDefault(x => x.Id == userId);
            if (user == null || user.Company == null) return new ApiResponse(500, "Ошибка данных.");

            DisabledTime disabledTime = context.DisabledTimes.Include(x => x.Branch).Include(x => x.Branch.Company).FirstOrDefault(x => x.Id == timeId);
            if (disabledTime == null) return new ApiResponse(500, "Неверный идентификатор.");

            if (disabledTime.Branch.Company.Id != user.Company.Id) return new ApiResponse(500, "Нет доступа.");
            if (disabledTime.TimeEnd < DateTime.Now) return new ApiResponse(500, "Нельзя удалить данные из истории!");

            context.DisabledTimes.Remove(disabledTime);
            context.SaveChanges();

            List<DisabledTime> disabledTimes = context.DisabledTimes.Include(x => x.Branch).Include(x => x.Branch.Company)
                                .Where(x => x.Branch.Id == disabledTime.Branch.Id && x.Branch.Company.Id == user.Company.Id).ToList();
            return new ApiResponse(disabledTimes.Select(x => new DisabledTime_DTO(x)));
        }
    }
}