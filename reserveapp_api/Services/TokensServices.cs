using reserveapp_api.Models;
using reserveapp_api.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

namespace reserveapp_api.Controllers.API
{
    public class TokensServices
    {
        //Структура JWT Token xxxxx.yyyyy.zzzzz (Header, Payload, Signature)

        static Dictionary<string, string> ParseToken(HttpRequest Request)
        {
            string authorization = Request.Headers["Authorization"];
            if (authorization.StartsWith("Bearer ", StringComparison.OrdinalIgnoreCase))
            {
                //Строка токена
                var encodedToken = authorization.Substring("Bearer ".Length).Trim();
                //зашифрованный Payload в токене 
                var encodedPayload = encodedToken.Split(".")[1];
                //Декодирование Payload
                var decodedPayload = Base64UrlEncoder.Decode(encodedPayload);
                //convert json to key value pair (Представление Payload в виде Dictionaries)
                return JsonConvert.DeserializeObject<Dictionary<string, string>>(decodedPayload);
            }
            return null;
        }

        public static long FindUserIdInToken(AppDbContext context, HttpRequest Request)
        {
            Dictionary<string, string> values = ParseToken(Request);
            if (values == null && !values.Keys.Contains("email"))
                return -1;
            //Поиск пользователя по Email
            User user = context.Users.Where(x => x.Email == values["email"]).First();
            return user.Id;
        }

        public static object CreateToken(User user, AppDbContext context)
        {
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            SymmetricSecurityKey key = AuthOptions.GetSymmetricSecurityKey();
            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                //Payload of JWT Token
                Subject = new ClaimsIdentity(new List<Claim>() { new Claim(ClaimTypes.Email, user.Email) }),
                Expires = DateTime.UtcNow.AddYears(AuthOptions.LIFETIME),
                SigningCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature)
            };
            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
            //Запись токена пользователя в таблицу
            string tokenStringPresentation = tokenHandler.WriteToken(token);
            context.Tokens.Add(new UserToken { user = user, Token = tokenStringPresentation, DateIssue = DateTimeOffset.Now, DateEnd = DateTimeOffset.UtcNow.AddYears(AuthOptions.LIFETIME) });
            context.SaveChanges();
            return tokenHandler.WriteToken(token);
        }

        public static object RefreshToken(AppDbContext context, HttpRequest Request, UserToken oldToken)
        {
            Dictionary<string, string> values = ParseToken(Request);
            if (values == null && !values.Keys.Contains("email"))
                return -1;
            //Поиск пользователя по Email
            User user = context.Users.FirstOrDefault(x => x.Email == values["email"]);
            //Замена токена в таблице, удаление старого - выдача нового (Не при каждом входе, а если до конца токена осталось 5-10%)
            //Дата конца действия токена
            UserToken _oldToken = context.Tokens.FirstOrDefault(x => x.user.Id == user.Id && x.Token == oldToken.Token);
            if (_oldToken == null)
                return HttpStatusCode.Unauthorized;

            object token = null;
            DateTimeOffset DateEndToken = _oldToken.DateEnd;
            var difference = DateEndToken.Subtract(DateTimeOffset.Now);
            //Если до истечения токена < 30 дней, то заменить токен, иначе использовать старый токен
            if (difference.Days < 30)
            {
                context.Tokens.RemoveRange(context.Tokens.Where(x => x.user.Id == user.Id && x.Token == oldToken.Token).ToList());
                string tokenValue = GetEncodedStringToken(Request);
                List<UserToken> oldTokens = context.Tokens.Where(x => x.user.Id == user.Id && x.Token == tokenValue).ToList();
                context.Tokens.RemoveRange(oldTokens);
                context.SaveChanges();
                token = CreateToken(user, context);
            }
            else
                token = oldToken.Token;
            return token;
        }

        public static string GetEncodedStringToken(HttpRequest Request)
        {
            string authorization = Request.Headers["Authorization"];
            if (authorization.StartsWith("Bearer ", StringComparison.OrdinalIgnoreCase))
            {
                //Строка токена
                return authorization.Substring("Bearer ".Length).Trim();
            }
            return null;
        }
    }
}
