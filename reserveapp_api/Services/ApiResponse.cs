﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace reserveapp_api.Models
{
    public class ApiResponse
    {
        public int Code = 200;
        public string Message = "";
        public object Data = null;

        public ApiResponse() { }

        public ApiResponse(object data) {
            Data = data;
        }

        public ApiResponse(int statusCode, string message)
        {
            Code = statusCode;
            Message = message;
        }

        //public static IActionResult ObjectResult(int statusCode, string message)
        //{
        //    var result = new ObjectResult(new { Code = statusCode, Message = message });
        //    result.StatusCode = statusCode;
        //    return result;
        //}

        //public static IActionResult ObjectResult(ApiResponse response)
        //{
        //    var result = new ObjectResult(new { Code = response.Code, Message = response.Message });
        //    result.StatusCode = response.Code;
        //    return result;
        //}
    }
}

