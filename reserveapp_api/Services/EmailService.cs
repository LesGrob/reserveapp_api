using MimeKit;
using MailKit.Net.Smtp;
using System.Threading.Tasks;

namespace reserveapp_api.Services
{
    public class EmailService
    {
        public async Task SendEmailAsync(string email, string subject, string message)
        {
            MimeMessage mailMessage = new MimeMessage();
            mailMessage.From.Add(new MailboxAddress("Dengrad", "info@dengrad.com"));
            mailMessage.To.Add(new MailboxAddress(email));
            mailMessage.Subject = subject;

            mailMessage.Body = new TextPart("html") { Text = message };

            using (var client = new SmtpClient())
            {
                client.CheckCertificateRevocation = false;
                client.Connect("smtp.yandex.ru", 465, true);
                client.Authenticate("info@dengrad.com", "Dengrad2019!");
                client.Send(mailMessage);
                client.Disconnect(true);
            }
        }

        public static string EmailRegistrationText(string callbackUrl)
        {
            string str = string.Format(
                @"<table
                    style=""border-collapse: collapse;border-spacing: 0;border-radius: 6px; padding: 0; margin: 50px auto;width: 500px;box-shadow: 0px 2px 1px -1px rgba(0, 0, 0, 0.2), 0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 1px 3px 0px rgba(0, 0, 0, 0.12);"">
                    <tr width=""100%"" height=""57"">
                        <td valign=""top"" align=""left""
                            style=""border-top-left-radius: 6px;border-top-right-radius: 6px;background: #913349;padding: 12px 18px;text-align: center;"">

                            <img height=""37"" width=""149"" src=""https://dengrad.com/Resources/logo.png""
                                style=""font-weight: bold;font-size: 18px;color: #fff;vertical-align: top;"" data-title=""Dengrad"">
                        </td>
                    </tr>

                    <tr width=""100%"">
                        <td valign=""top"" align=""left""
                            style=""border-bottom-left-radius: 6px;border-bottom-right-radius: 6px;background:#fff;padding: 18px;"">
                            <p style=""font: 15px/1.25em 'Helvetica Neue', Arial, Helvetica;color: #333;""> Мы
                                готовы активировать ваш аккаунт. Всё, что осталось — это убедиться, что это
                                ваш адрес электронной почты. </p>

                            <p style=""font: 15px/1.25em 'Helvetica Neue', Arial, Helvetica;color: #333;"">

                                <a href=""{0}""
                                    style=""border-radius: 3px;-moz-border-radius: 3px;-webkit-border-radius: 3px;background: #4caf50;color: #fff;cursor: pointer;display: block;font-weight: 700;font-size: 16px;line-height: 1.25em;margin: 24px auto 24px;padding: 10px 18px;text-decoration: none;width: 180px;text-align: center;""
                                    target=""_blank"" rel="" noopener noreferrer""> Подтвердить адрес </a>

                            </p>

                            <p style=""font: 15px/1.25em 'Helvetica Neue', Arial, Helvetica;color: #939393;margin-bottom: 0;"">
                                Если вы не создавали аккаунт Dengrad, просто удалите это письмо и всё
                                останется так, как есть. </p>
                        </td>
                    </tr>
                </table>", callbackUrl);
            return str;
        }

        public static string EmailResetPasswordText(string email, string code, string callbackUrl)
        {
            string str = string.Format(
                @"<table
                    style=""border-collapse: collapse;border-spacing: 0;border-radius: 6px; padding: 0; margin: 50px auto;width: 500px;box-shadow: 0px 2px 1px -1px rgba(0, 0, 0, 0.2), 0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 1px 3px 0px rgba(0, 0, 0, 0.12);"">
                    <tr width=""100%"" height=""57"">
                        <td valign=""top"" align=""left""
                            style=""border-top-left-radius: 6px;border-top-right-radius: 6px;background: #913349;padding: 12px 18px;text-align: center;"">

                            <img height=""37"" width=""149"" src=""https://dengrad.com/Resources/logo.png""
                                style=""font-weight: bold;font-size: 18px;color: #fff;vertical-align: top;"" data-title=""Dengrad"">
                        </td>
                    </tr>

                    <tr width=""100%"">
                        <td valign=""top"" align=""left""
                            style=""border-bottom-left-radius: 6px;border-bottom-right-radius: 6px;background:#fff;padding: 18px;"">
                            <p style=""font: 15px/1.25em 'Helvetica Neue', Arial, Helvetica;color: #333;"">
                                Вы получили данное письмо, потому что на ваш Email {0} был запрошен сброс пароля
                            </p>
                            <p style=""font: 15px/1.25em 'Helvetica Neue', Arial, Helvetica;color: #333;"">
                                Для сброса пароля пройдите по ссылке и введите в поле код доступа: <b>{1}</b>. Ссылка: <a href=""{2}"">Сбросить пароль</a>
                            </p>
                            <p style=""font: 15px/1.25em 'Helvetica Neue', Arial, Helvetica;color: #939393;margin-bottom: 0;"">
                                Если вы не создавали аккаунт Dengrad, просто удалите это письмо и всё
                                останется так, как есть. </p>
                        </td>
                    </tr>
                </table>", email, code, callbackUrl);
            return str;
        }

        public static string EmailHeaderedText(string body)
        {
            string str = string.Format(
                @"<table
                    style=""border-collapse: collapse;border-spacing: 0;border-radius: 6px; padding: 0; margin: 50px auto;width: 500px;box-shadow: 0px 2px 1px -1px rgba(0, 0, 0, 0.2), 0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 1px 3px 0px rgba(0, 0, 0, 0.12);"">
                    <tr width=""100%"" height=""57"">
                        <td valign=""top"" align=""left""
                            style=""border-top-left-radius: 6px;border-top-right-radius: 6px;background: #913349;padding: 12px 18px;text-align: center;"">

                            <img height=""37"" width=""149"" src=""https://dengrad.com/Resources/logo.png""
                                style=""font-weight: bold;font-size: 18px;color: #fff;vertical-align: top;"" data-title=""Dengrad"">
                        </td>
                    </tr>

                    <tr width=""100%"">
                        <td valign=""top"" align=""left""
                            style=""border-bottom-left-radius: 6px;border-bottom-right-radius: 6px;background:#fff;padding: 18px;"">
                            <p style=""font: 15px/1.25em 'Helvetica Neue', Arial, Helvetica;color: #333;"">
                                {0}
                            </p>
                        </td>
                    </tr>
                </table>", body);
            return str;
        }
    }
}
