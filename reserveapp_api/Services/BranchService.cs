﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.EntityFrameworkCore;
using reserveapp_api.Controllers.API;
using reserveapp_api.DTO;
using reserveapp_api.Models;

namespace reserveapp_api.Services
{
    public class BranchService
    {
        private AppDbContext context;
        User user;
        public BranchService(AppDbContext dbContext, User user)
        {
            context = dbContext;
            this.user = user;
        }

        //Get branch
        public ApiResponse Branch(long branchId)
        {
            Branch_DTO branch = new Branch_DTO(context.Branches.Include(x => x.Tariff).Include(x => x.Schedule)
                                                .FirstOrDefault(x => x.Company.Id == user.Company.Id && x.Id == branchId));
            if (branch == null) return new ApiResponse(500, "Филиал не найден.");
            if (branch.Company.Id != user.Company.Id) return new ApiResponse(500, "Ошибка доступа!");

            branch.Schedule = Schedule(branch.Schedule);

            //Получаем два списка - новый плагин и текущий!
            foreach (var zone in context.Zones.Include(x => x.Branch).Where(x => x.Branch.Id == branch.Id))
            {
                Zone_DTO f = new Zone_DTO(zone);
                f.Tables.AddRange(context.Tables.Include(x => x.Zone).Where(x => x.Zone.Id == zone.Id));
                if (zone.Temporary) branch.NewZones.Add(f);
                else branch.Zones.Add(f);
            }
            Bill bill = context.Bills.OrderBy(x => x.Id).Include(x => x.Branch).Include(x => x.Tariff).LastOrDefault(x => x.Branch.Id == branch.Id);
            branch.Bill = bill;
            if (bill == null) branch.Status = BranchStatus.NotPaid;
            else branch.Status = bill.Date.AddMonths(bill.Tariff.Period) > DateTime.Now ? BranchStatus.Paid : BranchStatus.Expired;

            branch.Bot = context.Bots.FirstOrDefault(x => x.BranchId == branch.Id);

            return new ApiResponse(branch);
        }



        //Remove Branch
        public ApiResponse RemoveBranch(long branchId)
        {
            Branch branch = context.Branches.Include(x => x.Company).Include(x => x.Schedule).FirstOrDefault(x => x.Company.Id == user.Company.Id && x.Id == branchId);
            if (branch == null) return new ApiResponse(500, "Филиал не найден.");
            if (branch.Company.Id != user.Company.Id) return new ApiResponse(500, "Ошибка доступа!");

            ScheduleDaysRemove(branch.Schedule.Id);
            context.Schedules.Remove(context.Schedules.FirstOrDefault(x => x.Id == branch.Schedule.Id));

            foreach (var zone in context.Zones.Include(x => x.Branch).Where(x => x.Branch.Id == branch.Id))
            {
                foreach (var table in context.Tables.Include(x => x.Zone).Where(x => x.Zone.Id == zone.Id))
                    context.Tables.Remove(table);
                context.Zones.Remove(zone);
            }
            context.Branches.Remove(branch);
            context.SaveChanges();
            return new ApiResponse(branch);
        }



        //Post branch
        public ApiResponse Branch(Branch_UploadDTO newBranch)
        {
            Branch branch = newBranch.Id == 0 ? new Branch() : context.Branches.FirstOrDefault(x => x.Id == newBranch.Id);
            if (newBranch.Id != 0 && branch == null) return new ApiResponse(500, "Филиал не найден.");
            if (newBranch.Id != 0 && branch.Company.Id != user.Company.Id) return new ApiResponse(500, "Ошибка доступа!");

            Tariff tariff = context.Tariffs.FirstOrDefault(x => x.Id == newBranch.Tariff.Id);
            if (tariff == null) return new ApiResponse(500, "Тариф не найден.");

            if (!String.IsNullOrEmpty(newBranch.Phone) && !Regex.IsMatch(newBranch.Phone, "^$|^((\\+7|7|8)+([0-9]){10})$"))
                return new ApiResponse(500, "Неверный номер телефона телефон.");

            try
            {
                branch.Name = newBranch.Name;
                branch.Adress = newBranch.Adress;
                branch.Phone = newBranch.Phone;
                branch.Company = user.Company;
                branch.Tariff = tariff;
                branch.TelegramIsAvailiable = newBranch.TelegramIsAvailiable;

                //telegram bot
                var controller = new BotController(context);
                if (branch.TelegramIsAvailiable)
                    controller.AddTelegramBot(branch);
                else
                    controller.RemoveTelegramBot(branch);

                //Не обязательное изображение. Если есть - проверяем, если уже существует, то удаляем. Добавляем новый файл.
                if (!string.IsNullOrEmpty(newBranch.BranchImage))
                {
                    if (!string.IsNullOrEmpty(branch.ImagePath))
                        System.IO.File.Delete(Directory.GetCurrentDirectory() + "\\Resources\\BranchImages\\" + branch.ImagePath);

                    branch.Guid = Guid.NewGuid();

                    string extensionFile = "." + newBranch.BranchImageName.Split(".").Last();

                    var bytes = Convert.FromBase64String(newBranch.BranchImage);
                    if (bytes.Length > 0)
                    {
                        using (var stream = new FileStream(Path.Combine(Directory.GetCurrentDirectory(), "Resources", "BranchImages", branch.Guid + extensionFile), FileMode.Create))
                        {
                            stream.Write(bytes, 0, bytes.Length);
                            branch.ImagePath = branch.Guid + extensionFile;
                            stream.Flush();
                        }
                    }
                }


                //Логика следующая: если плагин новый или не оплачен, то редачим Zones. Если уже оплатили,
                //то можно добавить "следующий плагин", который сохраняется в NewZones. Новый плагин из NewZones
                //сохраняется в Zone после оплаты.
                if (newBranch.Id == 0) context.Branches.Add(branch);

                foreach (var zone in newBranch.Zones) zone.Temporary = false;
                foreach (var zone in newBranch.NewZones)
                {
                    zone.Id = 0;
                    zone.Temporary = true;
                    foreach (var table in zone.Tables) table.Id = 0;
                }

                //система, которая будет изменяет/удаляет/добавляет зоны и столы, а не удаляет все сразу
                ApiResponse diffMainZones = DiffZones(newBranch.Zones, branch, false);
                if (diffMainZones.Code != 200) return diffMainZones;

                ApiResponse diffNewZones = DiffZones(newBranch.NewZones, branch, true);
                if (diffNewZones.Code != 200) return diffNewZones;

                //Проверяем присланное расписание, приводим его из DTO и сохраняем по таблицам
                newBranch.Schedule.PresetSchedele();
                if (!newBranch.Schedule.CheckValid()) return new ApiResponse(500, "Неверное расписание заведения.");
                Schedule schedule = SaveSchedule(newBranch.Schedule);
                if (schedule == null) return new ApiResponse(500, "Ошибка расписания.");
                branch.Schedule = schedule;

                context.SaveChanges();

                return Branch(branch.Id);
            }
            catch (Exception ex)
            {
                return new ApiResponse(500, ex.Message); //"Ошибка при добавлении.");
            }
        }



        //Work with zones and tables
        public ApiResponse DiffZones(List<Zone_UploadDTO> zones, Branch branch, bool temporary)
        {
            var oldZones = context.Zones.Include(x => x.Branch).Where(x => x.Branch.Id == branch.Id && x.Temporary == temporary && !zones.Select(y => y.Id).Contains(x.Id)).ToArray();
            foreach (var oldZ in oldZones)
            {
                RemoveTables(oldZ);
                context.Zones.Remove(oldZ);
            }

            foreach (var zone in zones)
            {
                Zone z = zone.Id == 0 ? new Zone() : context.Zones.FirstOrDefault(x => x.Id == zone.Id);
                if (zone.Id != 0 && z == null) return new ApiResponse(500, "Этаж не найден.");

                if (String.IsNullOrEmpty(zone.Name)) return new ApiResponse(500, "Название зоны не может быть пустым.");

                z.ZoneClone(zone);
                z.Branch = branch;

                if (!Zone.CorrectColor(zone.Color)) return new ApiResponse(500, "Неверный цвет зоны.");
                if (!Zone.CorrectColor(zone.TableColor)) return new ApiResponse(500, "Неверный цвет зоны.");
                if (!Zone.CorrectColor(zone.TableBColor)) return new ApiResponse(500, "Неверный цвет зоны.");
                if (!Zone.CorrectColor(zone.TableActiveColor)) return new ApiResponse(500, "Неверный цвет зоны.");
                if (!Zone.CorrectColor(zone.TableActiveBColor)) return new ApiResponse(500, "Неверный цвет зоны.");
                if (!Zone.CorrectColor(zone.TableDisabledColor)) return new ApiResponse(500, "Неверный цвет зоны.");
                if (!Zone.CorrectColor(zone.TableDisabledBColor)) return new ApiResponse(500, "Неверный цвет зоны.");

                //Если файл изменился либо этаж новый
                if (z.Id == 0 || !zone.ImagePath.Contains(z.Guid.ToString()))
                {
                    if (string.IsNullOrEmpty(zone.ZoneImage)) return new ApiResponse(500, "Пустое изображение зоны.");

                    var directory = Directory.GetCurrentDirectory() + @"/Resources/ZoneImages/";
                    var filePath = z.Guid + ".*";
                    string[] allFiles = Directory.GetFiles(directory, filePath);
                    foreach (var file in allFiles)
                        System.IO.File.Delete(file);
                    z.Guid = Guid.NewGuid();
                    string extensionFile = "." + zone.ZoneImageName.Split(".").Last();
                    var bytes = Convert.FromBase64String(zone.ZoneImage);
                    if (bytes.Length > 0)
                    {
                        try
                        {
                            using (var stream = new FileStream(Path.Combine(Directory.GetCurrentDirectory(), "Resources", "ZoneImages", z.Guid + extensionFile), FileMode.Create))
                            {
                                stream.Write(bytes, 0, bytes.Length);
                                z.ImagePath = z.Guid + extensionFile;
                                stream.Flush();
                            }
                        }
                        catch (Exception ex)
                        {
                            return new ApiResponse(500, ex.Message);
                        }
                    }
                }

                if (z.Id == 0) context.Zones.Add(z);

                ApiResponse result = DiffTables(zone.Tables, z);
                if (result.Code != 200) return result;
            }

            return new ApiResponse();
        }

        public ApiResponse DiffTables(List<Table_UploadDTO> tables, Zone zone)
        {
            var oldTables = context.Tables.Include(x => x.Zone).Where(x => x.Zone.Id == zone.Id && !tables.Select(y => y.Id).Contains(x.Id));
            context.Tables.RemoveRange(oldTables);

            foreach (var table in tables)
            {
                Table t = table.Id == 0 ? new Table() : context.Tables.FirstOrDefault(x => x.Id == table.Id);
                if (table.Id != 0 && t == null) return new ApiResponse(500, "Стол не найден.");

                if (table.Id != 0 && table.Deleted)
                {
                    context.Tables.Remove(t);
                }
                else
                {
                    t.TableClone(table);
                    t.Zone = zone;
                    if (table.Id == 0) { context.Tables.Add(t); }
                }
            }

            return new ApiResponse(200, "");
        }

        public void RemoveTables(Zone newZone)
        {
            if (newZone.Id == 0) return;
            List<Table> tables = context.Tables.Include(x => x.Zone).Where(x => x.Zone.Id == newZone.Id).ToList();
            context.Tables.RemoveRange(tables);
        }



        //Work with schedule
        public Schedule SaveSchedule(Schedule_DTO newSchedule)
        {
            Schedule schedule;

            if (newSchedule.Id == 0)
            {
                schedule = new Schedule();
                context.Schedules.Add(schedule);
            }
            else
            {
                schedule = context.Schedules.FirstOrDefault(x => x.Id == newSchedule.Id);
                ScheduleDaysRemove(schedule.Id);
            }

            schedule.ReservationLength = newSchedule.ReservationLength;
            foreach (var day in newSchedule.Days)
            {
                ScheduleDay scheduleDay = new ScheduleDay(schedule, day.WeekDay, day.IsActive);
                context.ScheduleDays.Add(scheduleDay);
                foreach (var interval in day.Intervals)
                {
                    DayInterval dayInterval = new DayInterval(scheduleDay, interval.Start, interval.End);
                    context.DayIntervals.Add(dayInterval);
                }
            }

            return schedule;
        }

        private Schedule_DTO Schedule(Schedule_DTO schedule)
        {
            schedule.Days = context.ScheduleDays.Where(x => x.Schedule.Id == schedule.Id)
                                                .Select(x => new ScheduleDay_DTO(x)).ToList();
            foreach (var day in schedule.Days)
                day.Intervals = context.DayIntervals.Where(x => x.ScheduleDay.Id == day.Id)
                                                    .Select(x => new DayInterval_DTO(x)).ToList();
            return schedule;
        }

        private void ScheduleDaysRemove(long scheduleId)
        {
            foreach (var day in context.ScheduleDays.Where(x => x.Schedule.Id == scheduleId))
            {
                context.DayIntervals.RemoveRange(context.DayIntervals.Where(x => x.ScheduleDay.Id == day.Id));
                context.ScheduleDays.Remove(day);
            }
        }



        //Checks
        private bool BranchPaid(long branchId)
        {
            Bill oldBill = context.Bills.OrderBy(x => x.Id).Include(x => x.Branch).Include(x => x.Tariff).LastOrDefault(x => x.Branch.Id == branchId);
            if (oldBill == null) return false;

            DateTime date = oldBill.Date;
            return date.AddMonths(oldBill.Tariff.Period) > DateTime.Now;
        }
    }
}
