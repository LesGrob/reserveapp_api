FROM microsoft/dotnet:2.1-sdk AS server
WORKDIR /app
COPY reserveapp_api/ .
RUN dotnet publish "reserveapp_api.csproj" -c Release -o out

FROM microsoft/dotnet:2.1-aspnetcore-runtime
RUN apt-get update
EXPOSE 80
WORKDIR /app
COPY --from=server /app/out .
ENTRYPOINT ["/bin/bash", "-c", "dotnet reserveapp_api.dll"]
